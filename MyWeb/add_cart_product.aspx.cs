﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb
{
    public partial class add_cart_product : System.Web.UI.Page
    {
        public string strID = "";
        string lang = "vi";
        public string strPrice = "0";
        DataTable cartdt = new DataTable();
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lang = MyWeb.Global.GetLang();
                if (Request["id"] != null) { strID = Request["id"].ToString(); }
                if (Session["prcart"] != null) 
                { 
                    cartdt = (DataTable)Session["prcart"]; 
                } else { 
                    Session["prcart"] = tbShopingcartDB.Cart_CreateCartDetail(); 
                    cartdt = (DataTable)Session["prcart"]; 
                }

                if (GlobalClass.chkLogin == "1")
                {
                    if (Session["uidr"] != null)
                    {
                        ltrThanhToan.Text = "<a class=\"btn-add-card-op order-now\" href=\"/thanh-toan.html\" title=\"" + MyWeb.Global.GetLangKey("button-order") + "\"><span>" + MyWeb.Global.GetLangKey("button-order") + "</span></a>";
                    }
                    else
                    {
                        ltrThanhToan.Text = "<a class=\"btn-add-card-op order-now\" href=\"javascript:{}\" onclick=\"clickOrder()\" title=\"" + MyWeb.Global.GetLangKey("button-order") + "\"><span>" + MyWeb.Global.GetLangKey("button-order") + "</span></a>";
                    }
                }
                else
                {
                    ltrThanhToan.Text = "<a class=\"btn-add-card-op order-now\" href=\"/thanh-toan.html\" title=\"" + MyWeb.Global.GetLangKey("button-order") + "\"><span>" + MyWeb.Global.GetLangKey("button-order") + "</span></a>";
                }

                if (!IsPostBack)
                {
                    try
                    {
                        AddiTemCart();
                        BinDataV2();
                    }
                    catch { }
                }
            }
            catch{}
            
        }

        void AddiTemCart()
        {
            if (Request["id"] != null)
            {
                List<tbProductDATA> objList = tbProductDB.tbProduct_GetByID(strID);
                if (objList.Count > 0)
                {
                    string strStatus = objList[0].proStatus.ToString();
                    if (strStatus.Contains("0"))
                    {
                        if (objList[0].proPrice != "")
                        {
                            strPrice = objList[0].proPrice;
                        }
                        tbShopingcartDB.Cart_AddProduct(ref cartdt, objList[0].proId.ToString(), "0", objList[0].proName, strPrice, "1", "1", objList[0].proImage.Split(Convert.ToChar(","))[0]);
                        objList.Clear();
                        objList = null;
                    }
                    else
                    {
                        ltrErr.Text = "Sản phẩm này đã hết bạn vui lòng đặt mua sản phẩm khác!";
                    }
                }
            }
        }

        /*Bind kiểu cũ
        void BindData()
        {
            if (cartdt.Rows.Count > 0)
            {
                rptDanhsach.DataSource = cartdt;
                rptDanhsach.DataBind();
                Double Tongtien = 0;
                for (int i = 0; i < cartdt.Rows.Count; i++)
                {
                    Tongtien = Tongtien + Convert.ToDouble(cartdt.Rows[i]["money"].ToString());
                }
                lblTongtien.Text = common.FormatNumber(Tongtien.ToString()) + " VNĐ";
                Session["prcart"] = cartdt;
            }
            else
            {
                rptDanhsach.DataSource = null;
                rptDanhsach.DataBind();
                lblTongtien.Text = "";
            }
            for (int i = 0; i < cartdt.Rows.Count; i++)
            {
                Label stt = (Label)rptDanhsach.Items[i].FindControl("lblstt");
                stt.Text = (i + 1).ToString();
            }
        }        

        protected void rptDanhsach_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            DataTable dt = new DataTable();
            string strName = e.CommandName.ToString();
            strID = e.CommandArgument.ToString();
            string surl = Request.Url.ToString();
            int index = e.Item.ItemIndex;
            switch (strName)
            {
                case "Update":
                    TextBox txtSL = (TextBox)rptDanhsach.Items[index].FindControl("txtSL");
                    if (txtSL.Text != "" && txtSL.Text != "0")
                    {
                        List<tbProductDATA> list = tbProductDB.tbProduct_GetByID(strID);
                        if (list.Count > 0)
                        {
                            tbShopingcartDB.Cart_UpdateNumber(ref cartdt, list[0].proId.ToString(), txtSL.Text);
                        }
                        BindData();
                    }
                    break;
                case "Delete":
                    dt = cartdt;
                    List<tbProductDATA> list2 = tbProductDB.tbProduct_GetByID(strID);
                    tbShopingcartDB.Cart_DeleteProduct(ref dt, list2[0].proId);
                    cartdt = dt;
                    Session["prcart"] = cartdt;
                    BindData();
                    break;
            }
        }
        */

        void BinDataV2()
        {
            string strReturn = "";
            if (cartdt.Rows.Count > 0)
            {
                Double Tongtien = 0;
                int i=1;
                foreach (DataRow r in cartdt.Rows)
                {
                    strReturn += "<tr>";
                    strReturn += "<td>" + i + "</td>";
                    strReturn += "<td style=\"text-align: left;\"><img class=\"view-img-cart\" src=" + r["img"] + " /> " + r["proname"] + "</td>";
                    strReturn += "<td>";
                    strReturn += "<input type=\"text\" class=\"input-card boder-color sl-" + r["proId"] + "\" id=\"txtSL-" + r["proId"] + "\" value=\"" + r["number"] + "\"/>";
                    strReturn += "<span class=\"btn btn-success btn-xs btn-add-card-btn\" id=\"btnEditClient\" title=\"Cập nhật sản phẩm\" onclick=\"UpdateProCart(" + r["proId"] + ")\"><i class=\"fa fa-pencil-square-o\"></i>Cập nhập</span>";
                    strReturn += "</td>";
                    strReturn += "<td>" + common.FormatNumber(r["price"].ToString()) + " VNĐ</td>";
                    strReturn += "<td>" + common.FormatNumber(r["money"].ToString()) + " VNĐ</td>";
                    strReturn += "<td><span class=\"btn btn-danger btn-xs btn-add-card-btn\" id=\"btnDel\" title=\"Xóa\" onclick=\"DeleteProCart(" + r["proId"] + ")\"><i class=\"fa fa-trash-o\"></i>" + MyWeb.Global.GetLangKey("cart_remove") + "</span></td>";
                    strReturn += "</tr>";
                    Tongtien = Tongtien + Convert.ToDouble(r["money"].ToString());
                }
                strReturn += "<tr>";
                strReturn += "<td colspan=\"6\" class=\"view-price\">" + MyWeb.Global.GetLangKey("cart_total_pay") + "<span> " + common.FormatNumber(Tongtien.ToString()) + " VNĐ</span></td>";
                strReturn += "</tr>";

                Session["prcart"] = cartdt;
            }
            else
            {
                strReturn += "<tr>";
                strReturn += "<td colspan=\"6\" class=\"view-price\">" + MyWeb.Global.GetLangKey("cart_total_pay") + "<span></span></td>";
                strReturn += "</tr>";
            }
            ltrCartBody.Text = strReturn;
        }

        public string GenBodyCart(DataTable cartdtx)
        {
            string strReturn = "";
            if (cartdtx.Rows.Count > 0)
            {
                Double Tongtien = 0;
                int i = 1;
                foreach (DataRow r in cartdtx.Rows)
                {
                    strReturn += "<tr>";
                    strReturn += "<td>" + i + "</td>";
                    strReturn += "<td style=\"text-align: left;\"><img class=\"view-img-cart\" src=" + r["img"] + " /> " + r["proname"] + "</td>";
                    strReturn += "<td>";
                    strReturn += "<input type=\"text\" class=\"input-card boder-color sl-" + r["proId"] + "\" id=\"txtSL-" + r["proId"] + "\" value=\"" + r["number"] + "\"/>";
                    strReturn += "<span class=\"btn btn-success btn-xs btn-add-card-btn\" id=\"btnEditClient\" title=\"Cập nhật sản phẩm\" onclick=\"UpdateProCart(" + r["proId"] + ")\"><i class=\"fa fa-pencil-square-o\"></i>Cập nhập</span>";
                    strReturn += "</td>";
                    strReturn += "<td>" + common.FormatNumber(r["price"].ToString()) + " VNĐ</td>";
                    strReturn += "<td>" + common.FormatNumber(r["money"].ToString()) + " VNĐ</td>";
                    strReturn += "<td><span class=\"btn btn-danger btn-xs btn-add-card-btn\" id=\"btnDel\" title=\"Xóa\" onclick=\"DeleteProCart(" + r["proId"] + ")\"><i class=\"fa fa-trash-o\"></i>" + MyWeb.Global.GetLangKey("cart_remove") + "</span></td>";
                    strReturn += "</tr>";
                    Tongtien = Tongtien + Convert.ToDouble(r["money"].ToString());
                }
                strReturn += "<tr>";
                strReturn += "<td colspan=\"6\" class=\"view-price\">" + MyWeb.Global.GetLangKey("cart_total_pay") + "<span> " + common.FormatNumber(Tongtien.ToString()) + " VNĐ</span></td>";
                strReturn += "</tr>";
            }
            else
            {
                strReturn += "<tr>";
                strReturn += "<td colspan=\"6\" class=\"view-price\">" + MyWeb.Global.GetLangKey("cart_total_pay") + "<span></span></td>";
                strReturn += "</tr>";
            }
            return strReturn;
        }

        [WebMethod]
        public static string Update(string sl, string id)
        {
            var a = new add_cart_product();
            if (a.Session["prcart"] != null)
            {
                a.cartdt = (DataTable)a.Session["prcart"];
            }
            else
            {
                a.Session["prcart"] = tbShopingcartDB.Cart_CreateCartDetail();
                a.cartdt = (DataTable)a.Session["prcart"];
            }
            if (sl != "" && sl != "0")
            {
                List<tbProductDATA> list = tbProductDB.tbProduct_GetByID(id);
                if (list.Count > 0)
                {
                    tbShopingcartDB.Cart_UpdateNumber(ref a.cartdt, id, sl);
                    a.Session["prcart"] = a.cartdt;
                }                
            }

            return a.GenBodyCart(a.cartdt);;
        }

        [WebMethod]
        public static string Delete(string id)
        {
            var a = new add_cart_product();
            if (a.Session["prcart"] != null)
            {
                a.cartdt = (DataTable)a.Session["prcart"];
            }
            else
            {
                a.Session["prcart"] = tbShopingcartDB.Cart_CreateCartDetail();
                a.cartdt = (DataTable)a.Session["prcart"];
            }
            DataTable dt = new DataTable();
            dt = a.cartdt;
            List<tbProductDATA> list2 = tbProductDB.tbProduct_GetByID(id);
            if (list2.Count > 0)
            {
                tbShopingcartDB.Cart_DeleteProduct(ref dt, id);
                a.cartdt = dt;
                a.Session["prcart"] = a.cartdt;
            }
            return a.GenBodyCart(a.cartdt);
        }
    }
}