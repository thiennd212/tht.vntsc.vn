﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class tbOnlineDB
{
    #region[tbOnline_Add]
    public static bool tbOnline_Add(tbOnlineDATA _tbOnlineDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbOnline_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@onName", _tbOnlineDATA.onName));
                dbCmd.Parameters.Add(new SqlParameter("@onName1", _tbOnlineDATA.onName1));
                dbCmd.Parameters.Add(new SqlParameter("@onTell", _tbOnlineDATA.onTell));
                dbCmd.Parameters.Add(new SqlParameter("@onNick", _tbOnlineDATA.onNick));
                dbCmd.Parameters.Add(new SqlParameter("@onActive", _tbOnlineDATA.onActive));
                dbCmd.Parameters.Add(new SqlParameter("@onOrd", _tbOnlineDATA.onOrd));
                dbCmd.Parameters.Add(new SqlParameter("@onType", _tbOnlineDATA.onType));
                dbCmd.Parameters.Add(new SqlParameter("@onLang", _tbOnlineDATA.onLang));
                dbCmd.Parameters.Add(new SqlParameter("@onSkype", _tbOnlineDATA.onSkype));
                dbCmd.Parameters.Add(new SqlParameter("@onEmail", _tbOnlineDATA.onEmail));
                dbCmd.Parameters.Add(new SqlParameter("@onYahoo", _tbOnlineDATA.onYahoo));
                dbCmd.Parameters.Add(new SqlParameter("@onFace", _tbOnlineDATA.onFace));
                dbCmd.Parameters.Add(new SqlParameter("@onZalo", _tbOnlineDATA.onZalo));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbOnline_Update]
    public static bool tbOnline_Update(tbOnlineDATA _tbOnlineDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbOnline_Update", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@onId", _tbOnlineDATA.onId));
                dbCmd.Parameters.Add(new SqlParameter("@onName", _tbOnlineDATA.onName));
                dbCmd.Parameters.Add(new SqlParameter("@onName1", _tbOnlineDATA.onName1));
                dbCmd.Parameters.Add(new SqlParameter("@onTell", _tbOnlineDATA.onTell));
                dbCmd.Parameters.Add(new SqlParameter("@onNick", _tbOnlineDATA.onNick));
                dbCmd.Parameters.Add(new SqlParameter("@onActive", _tbOnlineDATA.onActive));
                dbCmd.Parameters.Add(new SqlParameter("@onOrd", _tbOnlineDATA.onOrd));
                dbCmd.Parameters.Add(new SqlParameter("@onType", _tbOnlineDATA.onType));
                dbCmd.Parameters.Add(new SqlParameter("@onLang", _tbOnlineDATA.onLang));
                dbCmd.Parameters.Add(new SqlParameter("@onSkype", _tbOnlineDATA.onSkype));
                dbCmd.Parameters.Add(new SqlParameter("@onEmail", _tbOnlineDATA.onEmail));
                dbCmd.Parameters.Add(new SqlParameter("@onYahoo", _tbOnlineDATA.onYahoo));
                dbCmd.Parameters.Add(new SqlParameter("@onFace", _tbOnlineDATA.onFace));
                dbCmd.Parameters.Add(new SqlParameter("@onZalo", _tbOnlineDATA.onZalo));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbOnline_Delete]
    public static bool tbOnline_Delete(string sonId)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbOnline_Delete", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@onId", sonId));
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbOnline_GetByLang]
    public static List<tbOnlineDATA> tbOnline_GetByLang(string sonLang)
    {
        List<tbOnlineDATA> list = new List<tbOnlineDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbOnline_GetByLang", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@onLang", sonLang));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbOnlineDATA objtbOnlineDATA = new tbOnlineDATA(
                        reader["onId"].ToString(),
                        reader["onName"].ToString(),
                        reader["onName1"].ToString(),
                        reader["onTell"].ToString(),
                        reader["onNick"].ToString(),
                        reader["onActive"].ToString(),
                        reader["onOrd"].ToString(),
                        reader["onType"].ToString(),
                        reader["onLang"].ToString(),
                        reader["onSkype"].ToString(),
                        reader["onEmail"].ToString(),
                        reader["onYahoo"].ToString(),
                        reader["onFace"].ToString(),
                        reader["onZalo"].ToString()
                        );
                        list.Add(objtbOnlineDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbOnline_GetByID]
    public static List<tbOnlineDATA> tbOnline_GetByID(string sonId)
    {
        List<tbOnlineDATA> list = new List<tbOnlineDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbOnline_GetByID", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@onId", sonId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbOnlineDATA objtbOnlineDATA = new tbOnlineDATA(
                        reader["onId"].ToString(),
                        reader["onName"].ToString(),
                        reader["onName1"].ToString(),
                        reader["onTell"].ToString(),
                        reader["onNick"].ToString(),
                        reader["onActive"].ToString(),
                        reader["onOrd"].ToString(),
                        reader["onType"].ToString(),
                        reader["onLang"].ToString(),
                        reader["onSkype"].ToString(),
                        reader["onEmail"].ToString(),
                        reader["onYahoo"].ToString(),
                        reader["onFace"].ToString(),
                        reader["onZalo"].ToString());
                        list.Add(objtbOnlineDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbOnline_GetBygID]
    public static List<tbOnlineDATA> tbOnline_GetBygID(string onType)
    {
        List<tbOnlineDATA> list = new List<tbOnlineDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbOnline_GetBygID", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@onType", onType));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbOnlineDATA objtbOnlineDATA = new tbOnlineDATA(
                        reader["onId"].ToString(),
                        reader["onName"].ToString(),
                        reader["onName1"].ToString(),
                        reader["onTell"].ToString(),
                        reader["onNick"].ToString(),
                        reader["onActive"].ToString(),
                        reader["onOrd"].ToString(),
                        reader["onType"].ToString(),
                        reader["onLang"].ToString(),
                        reader["onSkype"].ToString(),
                        reader["onEmail"].ToString(),
                        reader["onYahoo"].ToString(),
                        reader["onFace"].ToString(),
                        reader["onZalo"].ToString()
                        );
                        list.Add(objtbOnlineDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
}