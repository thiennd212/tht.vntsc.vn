﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class tbOrdersDATA 
{
    #region[Declare variables]
    private string _id; 
    private string _Code;
    private string _Description;
    private string _AddressFrom;
    private string _AddressTo;
    private string _Status;
    private string _Lang;

    #endregion
    #region[Function]
    public tbOrdersDATA() { }
    public tbOrdersDATA(string id_, string Code_, string Description_, string AddressFrom_, string AddressTo_, string Status_, string Lang_)
    {
        _id = id_;
        _Code = Code_;
        _Description = Description_;
        _AddressFrom = AddressFrom_;
        _AddressTo = AddressTo_;
        _Status = Status_;
        _Lang = Lang_;
    }
    #endregion
    #region[Assigned value]
    public string id { get { return _id; } set { _id = value; } }
    public string Code { get { return _Code; } set { _Code = value; } }
    public string Description { get { return _Description; } set { _Description = value; } }
    public string AddressFrom { get { return _AddressFrom; } set { _AddressFrom = value; } }
    public string AddressTo { get { return _AddressTo; } set { _AddressTo = value; } }
    public string Status { get { return _Status; } set { _Status = value; } }
    public string Lang { get { return _Lang; } set { _Lang = value; } }
    #endregion
}