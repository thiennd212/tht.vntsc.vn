﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class tbOrdersDB 
{
    #region[tbOrders_Add]
    public static bool tbOrders_Add(tbOrdersDATA _tbOrdersDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbOrders_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@Code", _tbOrdersDATA.Code));
                dbCmd.Parameters.Add(new SqlParameter("@Description", _tbOrdersDATA.Description));
                dbCmd.Parameters.Add(new SqlParameter("@AddressFrom", _tbOrdersDATA.AddressFrom));
                dbCmd.Parameters.Add(new SqlParameter("@AddressTo", _tbOrdersDATA.AddressTo));
                dbCmd.Parameters.Add(new SqlParameter("@Status", _tbOrdersDATA.Status));
                dbCmd.Parameters.Add(new SqlParameter("@Lang", _tbOrdersDATA.Lang));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbOrders_Update]
    public static bool tbOrders_Update(tbOrdersDATA _tbOrdersDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbOrders_Update", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@id", _tbOrdersDATA.id));
                dbCmd.Parameters.Add(new SqlParameter("@Code", _tbOrdersDATA.Code));
                dbCmd.Parameters.Add(new SqlParameter("@Description", _tbOrdersDATA.Description));
                dbCmd.Parameters.Add(new SqlParameter("@AddressFrom", _tbOrdersDATA.AddressFrom));
                dbCmd.Parameters.Add(new SqlParameter("@AddressTo", _tbOrdersDATA.AddressTo));
                dbCmd.Parameters.Add(new SqlParameter("@Status", _tbOrdersDATA.Status));
                dbCmd.Parameters.Add(new SqlParameter("@Lang", _tbOrdersDATA.Lang));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbOrders_Delete]
    public static bool tbOrders_Delete(string sid)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbOrders_Delete", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@id", sid));
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbOrders_GetByAll]
    public static List<tbOrdersDATA> tbOrders_GetByAll()
    {
        List<tbOrdersDATA> list = new List<tbOrdersDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbOrders_GetByAll", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbOrdersDATA objtbOrdersDATA = new tbOrdersDATA(
                        reader["id"].ToString(),
                        reader["Code"].ToString(),
                        reader["Description"].ToString(),
                        reader["AddressFrom"].ToString(),
                        reader["AddressTo"].ToString(),
                        reader["Status"].ToString(),
                        reader["Lang"].ToString());
                        list.Add(objtbOrdersDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    //public static List<tbOrdersDATA> tbOrders_WhereByLinq(string where)
    //{
    //    List<tbOrdersDATA> list = new List<tbOrdersDATA>();
    //    using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
    //    {
    //        using (SqlCommand dbCmd = new SqlCommand("tbOrders_WhereByLinq", dbConn))
    //        {
    //            dbCmd.CommandType = CommandType.StoredProcedure;
    //            dbCmd.Parameters.Add(new SqlParameter("@where", where));
    //            dbConn.Open();
    //            using (SqlDataReader reader = dbCmd.ExecuteReader())
    //            {
    //                while (reader.Read())
    //                {
    //                    tbOrdersDATA obj = new tbOrdersDATA();
    //                    obj.id = reader["id"].ToString();
    //                    obj.Description = reader["Desription"].ToString();
    //                    obj.AddressFrom = reader["AddressFrom"].ToString();
    //                    obj.AddressTo = reader["AddressTo"].ToString();
    //                    obj.Status = reader["Status"].ToString();
    //                    obj.Lang = reader["Lang"].ToString();
    //                    list.Add(obj);
    //                }
    //            }
    //            dbConn.Close();
    //        }
    //    }
    //    return list;
    //}
    #endregion
    #region[tbOrders_GetByID]
    public static List<tbOrdersDATA> tbOrders_GetByID(string sid)
    {
        List<tbOrdersDATA> list = new List<tbOrdersDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbOrders_GetByID", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@id", sid));
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbOrdersDATA objtbOrdersDATA = new tbOrdersDATA(
                        reader["id"].ToString(),
                        reader["Code"].ToString(),
                        reader["Description"].ToString(),
                        reader["AddressFrom"].ToString(),
                        reader["AddressTo"].ToString(),
                        reader["Status"].ToString(),
                        reader["Lang"].ToString());
                        list.Add(objtbOrdersDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion

    #region[tbOrders_Status]
    public static bool tbOrders_UpdateStatus(tbOrdersDATA _tbOrdersDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbOrders_UpdateStatus", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@id", _tbOrdersDATA.id));
                dbCmd.Parameters.Add(new SqlParameter("@status", _tbOrdersDATA.Status));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
}