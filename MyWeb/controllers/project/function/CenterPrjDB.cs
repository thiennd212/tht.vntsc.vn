﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class CenterPrjDB
{
    #region[Center_Add]
    public static bool Center_Add(CenterPrjDATA _CenterPrjDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_Center_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@catName", _CenterPrjDATA.CatId));
                dbCmd.Parameters.Add(new SqlParameter("@catImage", _CenterPrjDATA.ProId));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[Center_Add]
    public static bool Center_Add2(CenterPrjDATA _CenterPrjDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_Center_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@catName", _CenterPrjDATA.CatId));
                dbCmd.Parameters.Add(new SqlParameter("@catImage", _CenterPrjDATA.ProId));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[Center_GetByID]
    public static List<CenterPrjDATA> Center_GetByID(string Id)
    {
        List<CenterPrjDATA> list = new List<CenterPrjDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_Center_GetByID", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@Id", Id));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        CenterPrjDATA objCenterPrjDATA = new CenterPrjDATA(
                            reader["Id"].ToString(),
                            reader["CatId"].ToString(),
                            reader["ProId"].ToString());
                        list.Add(objCenterPrjDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[Center_Insert]
    public static bool Center_Insert(string CatId, string proId)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("Insert into Center(CatId, ProId) VALUES(" + CatId + ", " + proId + ")", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[Center_Update]
    public static bool Center_Update(string CatId, string proId)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("Update Center set CatId = " + CatId + " where ProId = " + proId + "", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion

    #region[Center_GetByProIdAndCatId]
    public static List<CenterPrjDATA> Center_GetByProIdAndCatId(string proId, string catId)
    {
        List<CenterPrjDATA> list = new List<CenterPrjDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_Center_GetByProIDAndCatId", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@ProId", proId));
                dbCmd.Parameters.Add(new SqlParameter("@CatId", catId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        CenterPrjDATA objCenterPrjDATA = new CenterPrjDATA(
                            reader["Id"].ToString(),
                            reader["CatId"].ToString(),
                            reader["ProId"].ToString());
                        list.Add(objCenterPrjDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[sp_Center_GetProId]
    public static List<CenterPrjDATA> Center_GetProId(string proid)
    {
        List<CenterPrjDATA> list = new List<CenterPrjDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_Center_GetProId", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@ProId", proid));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        CenterPrjDATA objCenterPrjDATA = new CenterPrjDATA(
                            reader["Id"].ToString(),
                            reader["CatId"].ToString(),
                            reader["ProId"].ToString());
                        list.Add(objCenterPrjDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[Center_GetByAll]
    public static List<CenterPrjDATA> Center_GetByAll()
    {
        List<CenterPrjDATA> list = new List<CenterPrjDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_Center_GetByAll", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        CenterPrjDATA objCenterPrjDATA = new CenterPrjDATA(
                            reader["Id"].ToString(),
                            reader["CatId"].ToString(),
                            reader["ProId"].ToString());
                        list.Add(objCenterPrjDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[Center_Update]
    public static bool Center_Update(CenterPrjDATA _CenterPrjDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("SP_Center_Update", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@catId", _CenterPrjDATA.Id));
                dbCmd.Parameters.Add(new SqlParameter("@catName", _CenterPrjDATA.CatId));
                dbCmd.Parameters.Add(new SqlParameter("@catImage", _CenterPrjDATA.ProId));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[Center_Delete]
    public static bool Center_Delete(CenterPrjDATA _CenterPrjDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_Center_Delete", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@Id", _CenterPrjDATA.Id));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[Center_DeleteByProId]
    public static bool Center_DeleteByProId(string proId)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("delete from center where ProId = '" + proId + "'", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[AttproDelete]
    public static bool Attpro_delete(string attid)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("Delete tbAttPro where Id = " + attid + "", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[AttproDelete]
    public static bool Insert_attpro(string attid, string proid)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("Insert into tbAttPro(attId, proId) VALUES(" + attid + ", " + proid + ")", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
}