﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class tbAdvertiseDB
{
    #region[tbAdvertise_Add]
    public static bool tbAdvertise_Add(tbAdvertiseDATA _tbAdvertiseDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbAdvertise_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@advName", _tbAdvertiseDATA.advName));
                dbCmd.Parameters.Add(new SqlParameter("@advImage", _tbAdvertiseDATA.advImage));
                dbCmd.Parameters.Add(new SqlParameter("@advWidth", _tbAdvertiseDATA.advWidth));
                dbCmd.Parameters.Add(new SqlParameter("@advHeight", _tbAdvertiseDATA.advHeight));
                dbCmd.Parameters.Add(new SqlParameter("@advLink", _tbAdvertiseDATA.advLink));
                dbCmd.Parameters.Add(new SqlParameter("@advTarget", _tbAdvertiseDATA.advTarget));
                dbCmd.Parameters.Add(new SqlParameter("@advContent", _tbAdvertiseDATA.advContent));
                dbCmd.Parameters.Add(new SqlParameter("@advPosition", _tbAdvertiseDATA.advPosition));
                dbCmd.Parameters.Add(new SqlParameter("@pagId", _tbAdvertiseDATA.pagId));
                dbCmd.Parameters.Add(new SqlParameter("@advClick", _tbAdvertiseDATA.advClick));
                dbCmd.Parameters.Add(new SqlParameter("@advOrd", _tbAdvertiseDATA.advOrd));
                dbCmd.Parameters.Add(new SqlParameter("@advActive", _tbAdvertiseDATA.advActive));
                dbCmd.Parameters.Add(new SqlParameter("@advLang", _tbAdvertiseDATA.advLang));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbAdvertise_Update]
    public static bool tbAdvertise_Update(tbAdvertiseDATA _tbAdvertiseDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbAdvertise_Update", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@advId", _tbAdvertiseDATA.advId));
                dbCmd.Parameters.Add(new SqlParameter("@advName", _tbAdvertiseDATA.advName));
                dbCmd.Parameters.Add(new SqlParameter("@advImage", _tbAdvertiseDATA.advImage));
                dbCmd.Parameters.Add(new SqlParameter("@advWidth", _tbAdvertiseDATA.advWidth));
                dbCmd.Parameters.Add(new SqlParameter("@advHeight", _tbAdvertiseDATA.advHeight));
                dbCmd.Parameters.Add(new SqlParameter("@advLink", _tbAdvertiseDATA.advLink));
                dbCmd.Parameters.Add(new SqlParameter("@advTarget", _tbAdvertiseDATA.advTarget));
                dbCmd.Parameters.Add(new SqlParameter("@advContent", _tbAdvertiseDATA.advContent));
                dbCmd.Parameters.Add(new SqlParameter("@advPosition", _tbAdvertiseDATA.advPosition));
                dbCmd.Parameters.Add(new SqlParameter("@pagId", _tbAdvertiseDATA.pagId));
                dbCmd.Parameters.Add(new SqlParameter("@advClick", _tbAdvertiseDATA.advClick));
                dbCmd.Parameters.Add(new SqlParameter("@advOrd", _tbAdvertiseDATA.advOrd));
                dbCmd.Parameters.Add(new SqlParameter("@advActive", _tbAdvertiseDATA.advActive));
                dbCmd.Parameters.Add(new SqlParameter("@advLang", _tbAdvertiseDATA.advLang));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbAdvertise_Delete]
    public static bool tbAdvertise_Delete(string sadvId)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbAdvertise_Delete", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@advId", sadvId));
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbAdvertise_GetByAll]
    public static List<tbAdvertiseDATA> tbAdvertise_GetByAll(string sadvLang)
    {
        List<tbAdvertiseDATA> list = new List<tbAdvertiseDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbAdvertise_GetByAll", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@advLang", sadvLang));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbAdvertiseDATA objtbAdvertiseDATA = new tbAdvertiseDATA(
                        reader["advId"].ToString(),
                        reader["advName"].ToString(),
                        reader["advImage"].ToString(),
                        reader["advWidth"].ToString(),
                        reader["advHeight"].ToString(),
                        reader["advLink"].ToString(),
                        reader["advTarget"].ToString(),
                        reader["advContent"].ToString(),
                        reader["advPosition"].ToString(),
                        reader["pagId"].ToString(),
                        reader["advClick"].ToString(),
                        reader["advOrd"].ToString(),
                        reader["advActive"].ToString(),
                        reader["advLang"].ToString());
                        list.Add(objtbAdvertiseDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbAdvertise_GetByID]
    public static List<tbAdvertiseDATA> tbAdvertise_GetByID(string sadvId)
    {
        List<tbAdvertiseDATA> list = new List<tbAdvertiseDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbAdvertise_GetByID", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@advId", sadvId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbAdvertiseDATA objtbAdvertiseDATA = new tbAdvertiseDATA(
                        reader["advId"].ToString(),
                        reader["advName"].ToString(),
                        reader["advImage"].ToString(),
                        reader["advWidth"].ToString(),
                        reader["advHeight"].ToString(),
                        reader["advLink"].ToString(),
                        reader["advTarget"].ToString(),
                        reader["advContent"].ToString(),
                        reader["advPosition"].ToString(),
                        reader["pagId"].ToString(),
                        reader["advClick"].ToString(),
                        reader["advOrd"].ToString(),
                        reader["advActive"].ToString(),
                        reader["advLang"].ToString());
                        list.Add(objtbAdvertiseDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbAdvertise_GetBypagId]
    public static List<tbAdvertiseDATA> tbAdvertise_GetBypagId(string pagId)
    {
        List<tbAdvertiseDATA> list = new List<tbAdvertiseDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbAdvertise_GetBypagId", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@pagId", pagId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbAdvertiseDATA objtbAdvertiseDATA = new tbAdvertiseDATA(
                        reader["advId"].ToString(),
                        reader["advName"].ToString(),
                        reader["advImage"].ToString(),
                        reader["advWidth"].ToString(),
                        reader["advHeight"].ToString(),
                        reader["advLink"].ToString(),
                        reader["advTarget"].ToString(),
                        reader["advContent"].ToString(),
                        reader["advPosition"].ToString(),
                        reader["pagId"].ToString(),
                        reader["advClick"].ToString(),
                        reader["advOrd"].ToString(),
                        reader["advActive"].ToString(),
                        reader["advLang"].ToString());
                        list.Add(objtbAdvertiseDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbAdvertise_GetByChuoi]
    public static List<tbAdvertiseDATA> tbAdvertise_GetByChuoi(string Chuoi)
    {
        List<tbAdvertiseDATA> list = new List<tbAdvertiseDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("select * from tbAdvertise where 1=1 " + Chuoi + " order by advPosition,advOrd", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbAdvertiseDATA objtbAdvertiseDATA = new tbAdvertiseDATA(
                        reader["advId"].ToString(),
                        reader["advName"].ToString(),
                        reader["advImage"].ToString(),
                        reader["advWidth"].ToString(),
                        reader["advHeight"].ToString(),
                        reader["advLink"].ToString(),
                        reader["advTarget"].ToString(),
                        reader["advContent"].ToString(),
                        reader["advPosition"].ToString(),
                        reader["pagId"].ToString(),
                        reader["advClick"].ToString(),
                        reader["advOrd"].ToString(),
                        reader["advActive"].ToString(),
                        reader["advLang"].ToString());
                        list.Add(objtbAdvertiseDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbAdvertise_GetByadvPosition]
    public static List<tbAdvertiseDATA> tbAdvertise_GetByadvPosition(string sadvPosition, string langs)
    {
        List<tbAdvertiseDATA> list = new List<tbAdvertiseDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbAdvertise_GetByadvPosition", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@advPosition", sadvPosition));
                dbCmd.Parameters.Add(new SqlParameter("@advLang", langs));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbAdvertiseDATA objtbAdvertiseDATA = new tbAdvertiseDATA(
                        reader["advId"].ToString(),
                        reader["advName"].ToString(),
                        reader["advImage"].ToString(),
                        reader["advWidth"].ToString(),
                        reader["advHeight"].ToString(),
                        reader["advLink"].ToString(),
                        reader["advTarget"].ToString(),
                        reader["advContent"].ToString(),
                        reader["advPosition"].ToString(),
                        reader["pagId"].ToString(),
                        reader["advClick"].ToString(),
                        reader["advOrd"].ToString(),
                        reader["advActive"].ToString(),
                        reader["advLang"].ToString());
                        list.Add(objtbAdvertiseDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbAdvertise_GetByadvPositionTop5]
    public static List<tbAdvertiseDATA> tbAdvertise_GetByadvPositionTop5(string sadvPosition)
    {
        List<tbAdvertiseDATA> list = new List<tbAdvertiseDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbAdvertise_GetByadvPositionTop5", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@advPosition", sadvPosition));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbAdvertiseDATA objtbAdvertiseDATA = new tbAdvertiseDATA(
                        reader["advId"].ToString(),
                        reader["advName"].ToString(),
                        reader["advImage"].ToString(),
                        reader["advWidth"].ToString(),
                        reader["advHeight"].ToString(),
                        reader["advLink"].ToString(),
                        reader["advTarget"].ToString(),
                        reader["advContent"].ToString(),
                        reader["advPosition"].ToString(),
                        reader["pagId"].ToString(),
                        reader["advClick"].ToString(),
                        reader["advOrd"].ToString(),
                        reader["advActive"].ToString(),
                        reader["advLang"].ToString());
                        list.Add(objtbAdvertiseDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbAdvertise_GetByadvPositionTop1]
    public static List<tbAdvertiseDATA> tbAdvertise_GetByadvPositionTop1(string sadvPosition)
    {
        List<tbAdvertiseDATA> list = new List<tbAdvertiseDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbAdvertise_GetByadvPositionTop1", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@advPosition", sadvPosition));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbAdvertiseDATA objtbAdvertiseDATA = new tbAdvertiseDATA(
                        reader["advId"].ToString(),
                        reader["advName"].ToString(),
                        reader["advImage"].ToString(),
                        reader["advWidth"].ToString(),
                        reader["advHeight"].ToString(),
                        reader["advLink"].ToString(),
                        reader["advTarget"].ToString(),
                        reader["advContent"].ToString(),
                        reader["advPosition"].ToString(),
                        reader["pagId"].ToString(),
                        reader["advClick"].ToString(),
                        reader["advOrd"].ToString(),
                        reader["advActive"].ToString(),
                        reader["advLang"].ToString());
                        list.Add(objtbAdvertiseDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbAdvertise_GetByadvPositionCatIdTop1]
    public static List<tbAdvertiseDATA> tbAdvertise_GetByadvPositionCatIdTop1(string sadvPosition, string scatId, string advlangs)
    {
        List<tbAdvertiseDATA> list = new List<tbAdvertiseDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbAdvertise_GetByadvPositionCatIdTop1", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@advPosition", sadvPosition));
                dbCmd.Parameters.Add(new SqlParameter("@catId", scatId));
                dbCmd.Parameters.Add(new SqlParameter("@advLang", advlangs));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbAdvertiseDATA objtbAdvertiseDATA = new tbAdvertiseDATA(
                        reader["advId"].ToString(),
                        reader["advName"].ToString(),
                        reader["advImage"].ToString(),
                        reader["advWidth"].ToString(),
                        reader["advHeight"].ToString(),
                        reader["advLink"].ToString(),
                        reader["advTarget"].ToString(),
                        reader["advContent"].ToString(),
                        reader["advPosition"].ToString(),
                        reader["pagId"].ToString(),
                        reader["advClick"].ToString(),
                        reader["advOrd"].ToString(),
                        reader["advActive"].ToString(),
                        reader["advLang"].ToString());
                        list.Add(objtbAdvertiseDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbAdvertise_GetByadvPositionCatId]
    public static List<tbAdvertiseDATA> tbAdvertise_GetByadvPositionCatId(string sadvPosition, string scatId)
    {
        List<tbAdvertiseDATA> list = new List<tbAdvertiseDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbAdvertise_GetByadvPositionCatId", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@advPosition", sadvPosition));
                dbCmd.Parameters.Add(new SqlParameter("@catId", scatId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbAdvertiseDATA objtbAdvertiseDATA = new tbAdvertiseDATA(
                        reader["advId"].ToString(),
                        reader["advName"].ToString(),
                        reader["advImage"].ToString(),
                        reader["advWidth"].ToString(),
                        reader["advHeight"].ToString(),
                        reader["advLink"].ToString(),
                        reader["advTarget"].ToString(),
                        reader["advContent"].ToString(),
                        reader["advPosition"].ToString(),
                        reader["pagId"].ToString(),
                        reader["advClick"].ToString(),
                        reader["advOrd"].ToString(),
                        reader["advActive"].ToString(),
                        reader["advLang"].ToString());
                        list.Add(objtbAdvertiseDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbPage_GetBypagPosition_grnId]
    public static List<tbAdvertiseDATA> tbPage_GetBypagPosition_grnId(string pagPosition, string grnId)
    {
        List<tbAdvertiseDATA> list = new List<tbAdvertiseDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbPage_GetBypagPosition_grnId", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@pagPosition", pagPosition));
                dbCmd.Parameters.Add(new SqlParameter("@grnId", grnId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbAdvertiseDATA objtbAdvertiseDATA = new tbAdvertiseDATA(
                        reader["advId"].ToString(),
                        reader["advName"].ToString(),
                        reader["advImage"].ToString(),
                        reader["advWidth"].ToString(),
                        reader["advHeight"].ToString(),
                        reader["advLink"].ToString(),
                        reader["advTarget"].ToString(),
                        reader["advContent"].ToString(),
                        reader["advPosition"].ToString(),
                        reader["pagId"].ToString(),
                        reader["advClick"].ToString(),
                        reader["advOrd"].ToString(),
                        reader["advActive"].ToString(),
                        reader["advLang"].ToString());
                        list.Add(objtbAdvertiseDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbAdvertise_GetByadvPositionTop]
    public static List<tbAdvertiseDATA> tbAdvertise_GetByadvPositionTop(string top, string sadvPosition)
    {
        List<tbAdvertiseDATA> list = new List<tbAdvertiseDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("SELECT top " + top + " * FROM tbAdvertise where advPosition=" + sadvPosition + " and advActive=1 order by advOrd", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbAdvertiseDATA objtbAdvertiseDATA = new tbAdvertiseDATA(
                        reader["advId"].ToString(),
                        reader["advName"].ToString(),
                        reader["advImage"].ToString(),
                        reader["advWidth"].ToString(),
                        reader["advHeight"].ToString(),
                        reader["advLink"].ToString(),
                        reader["advTarget"].ToString(),
                        reader["advContent"].ToString(),
                        reader["advPosition"].ToString(),
                        reader["pagId"].ToString(),
                        reader["advClick"].ToString(),
                        reader["advOrd"].ToString(),
                        reader["advActive"].ToString(),
                        reader["advLang"].ToString());
                        list.Add(objtbAdvertiseDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbAdvertise_GetByPosition_Vietime]
    public static List<tbAdvertiseDATA> tbAdvertise_GetByPosition_Vietime(string advPosition)
    {
        List<tbAdvertiseDATA> list = new List<tbAdvertiseDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbAdvertise_GetByPosition_Vietime", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@advPosition", advPosition));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbAdvertiseDATA objtbAdvertiseDATA = new tbAdvertiseDATA(
                        reader["advId"].ToString(),
                        reader["advName"].ToString(),
                        reader["advImage"].ToString(),
                        reader["advWidth"].ToString(),
                        reader["advHeight"].ToString(),
                        reader["advLink"].ToString(),
                        reader["advTarget"].ToString(),
                        reader["advContent"].ToString(),
                        reader["advPosition"].ToString(),
                        reader["pagId"].ToString(),
                        reader["advClick"].ToString(),
                        reader["advOrd"].ToString(),
                        reader["advActive"].ToString(),
                        reader["advLang"].ToString());
                        list.Add(objtbAdvertiseDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbAdvertise_Page_Delete]
    public static bool tbAdvertise_Page_Delete(string sadvId)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbAdvertise_Page_Delete", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@advertisID", sadvId));
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
}