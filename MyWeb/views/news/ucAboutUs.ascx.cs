﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.news
{
    public partial class ucAboutUs : System.Web.UI.UserControl
    {
        string lang = "vi";
        public bool showNews = true;
        private string strNumberView = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                BindData();
            }
        }
        private void BindData()
        {
            tbNew news = db.tbNews.OrderByDescending(s => s.newDate).ThenByDescending(s => s.newid).FirstOrDefault(s => s.newIndex == 1 && s.newLang.Trim() == lang);
            if (news != null && news.newid > 0)
            {
                showNews = true;
                ltrName.Text = news.newName;
                ltrContent.Text = news.newDetail;
            }
        }
    }
}