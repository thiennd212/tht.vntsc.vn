﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.pages
{
    public partial class ucBookNow2 : System.Web.UI.UserControl
    {
        string lang = "vi"; 
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                #region[Sendmail]
                List<tbConfigDATA> list = tbConfigDB.tbConfig_GetLang(lang);
                string mailfrom = list[0].conMail_Noreply;
                string mailto = list[0].conMail_Info;
                string pas = list[0].conMail_Pass;
                string host = host = "smtp.gmail.com";
                if (list[0].conMail_Method.Length > 0)
                {
                    host = list[0].conMail_Method;
                }
                int post = 465;
                if (list[0].conMail_Port.Length > 0)
                {
                    post = int.Parse(list[0].conMail_Port);
                }
                string cc = "";
                string Noidung = "";
                Noidung += "<table>";
                Noidung += txtname.Text != "" ? "<tr><td><b>Họ tên:</b></td><td>" + txtname.Text + "</td></tr>" : "";
                Noidung += txtDienthoai.Text != "" ? "<tr><td><b>Điện thoại:</b></td><td>" + txtDienthoai.Text + "</td></tr>" : "";
                Noidung += txtmail.Text != "" ? "<tr><td><b>Email:</b></td><td>" + txtmail.Text + "</td></tr>" : "";
                Noidung += txtSoNguoi.Text != "" ? "<tr><td><b>Số người:</b></td><td>" + txtSoNguoi.Text + "</td></tr>" : "";
                Noidung += txtThoiGian.Text != "" ? "<tr><td><b>Thời gian:</b></td><td>" + txtThoiGian.Text + "</td></tr>" : "";
                Noidung += txtnoidung.Text != "" ? "<tr><td><b>Nội dung:</b></td><td>" + txtnoidung.Text + "</td></tr>" : "";
                Noidung += "</table>";

                try
                {
                    common.SendMail(mailto, cc, "", "", "Thông tin liên hệ từ " + txtname.Text, Noidung);
                    common.SendMail(txtmail.Text.Trim(), "", "", "", "Thông tin liên hệ trên website " + Request.Url.Host, Noidung);
                    lblthongbao.Text = "Bạn đã gửi thành công !!";
                }
                catch
                {
                    lblthongbao.Text = "Lỗi không gửi được liên hệ !!";
                }
                #endregion
                string file_path = "";
                tbContact ObjtbContact = new tbContact();
                ObjtbContact.conName = common.killChars(txtname.Text);
                ObjtbContact.conCompany = "";
                ObjtbContact.conAddress = "";
                ObjtbContact.conTel = common.killChars(txtDienthoai.Text);
                ObjtbContact.conMail = common.killChars(txtmail.Text);
                ObjtbContact.conDetail = common.killChars(txtnoidung.Text);
                ObjtbContact.conActive = 0;
                ObjtbContact.conLang = lang;
                ObjtbContact.conPositions = "";
                ObjtbContact.confax = "";
                ObjtbContact.confile = file_path;
                ObjtbContact.conwebsite = "";
                ObjtbContact.numberPeople = int.Parse(txtSoNguoi.Text);
                ObjtbContact.bookDate = txtThoiGian.Text;
                ObjtbContact.conType = 10;
                ObjtbContact.conDate = DateTime.Now;

                db.tbContacts.InsertOnSubmit(ObjtbContact);
                db.SubmitChanges();
                lblthongbao.Text = MyWeb.Global.GetLangKey("mess_contact");
                txtname.Text = "";
                txtDienthoai.Text = "";
                txtmail.Text = "";
                txtnoidung.Text = "";                
            }
        }

    }
}