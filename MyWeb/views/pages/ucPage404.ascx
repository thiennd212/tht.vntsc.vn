﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucPage404.ascx.cs" Inherits="MyWeb.views.pages.ucPage404" %>
<div class="box-404-sub">
    <div class="header">Không tìm thấy trang yêu cầu!</div>
    <div class="body-404">
        <asp:Literal ID="ltrModules" runat="server"></asp:Literal>
    </div>
</div>