﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProSearch.ascx.cs" Inherits="MyWeb.views.search.ucProSearch" %>
<div class="box-search-product">
    <div class="cate-header sub-top">
        <div class="txt-name-sub">
            <%= MyWeb.Global.GetLangKey("search_result") %>
        </div>
    </div>
    <div id="loadFillter">
        <asp:Literal ID="ltrlistpro" runat="server"></asp:Literal>
        <div class="clearfix">
            <asp:Literal ID="ltrPaging" runat="server"></asp:Literal>
        </div>
    </div>
</div>
