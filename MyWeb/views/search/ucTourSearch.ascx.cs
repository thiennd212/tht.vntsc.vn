﻿using MyWeb.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.search
{
    public partial class ucTourSearch : System.Web.UI.UserControl
    {
        string lang = "vi";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                LoadData();
            }
        }
        protected void LoadData()
        {
            string placeFrom = !String.IsNullOrEmpty(Request.QueryString["from"]) ? Request.QueryString["from"].ToString() : "";
            string placeTo = !String.IsNullOrEmpty(Request.QueryString["to"]) ? Request.QueryString["to"].ToString() : "";
            string time = !String.IsNullOrEmpty(Request.QueryString["time"]) ? Request.QueryString["time"].ToString() : "";
            string inout = !String.IsNullOrEmpty(Request.QueryString["inout"]) ? Request.QueryString["inout"].ToString() : "";
            int plFrom = 0;
            int plTo = 0;
            int inOut = 0;

            List<int?> lstCatInOutTop = new List<int?>();

            if (!string.IsNullOrEmpty(placeFrom))
                plFrom = Convert.ToInt32(placeFrom);
            if (!string.IsNullOrEmpty(placeTo))
                plTo = Convert.ToInt32(placeTo);
            if (!string.IsNullOrEmpty(inout))
                inOut = Convert.ToInt32(inout);
            
                time = time.Trim();

            if (inOut != 0)
            {
                var iCat = db.tbPages.FirstOrDefault(x => x.pagId == inOut);
                var inOutTop = db.tbPages.Where(
                                s =>
                                s.pagLang == lang && s.pagActive == 1
                                && s.pagType == int.Parse(pageType.TourPlace)
                                && s.paglevel.StartsWith(iCat.paglevel)).Select(x => x.pagId).ToList();
                if(inOutTop !=null && inOutTop.Count > 0)
                {
                    foreach(var item in inOutTop)
                    {
                        lstCatInOutTop.Add(item);
                    }
                }
                lstCatInOutTop.Add(inOut);
            }           

            int currentPage = Request.RawUrl.Contains("page") ? int.Parse(common.GetParameterFromUrl(Request.RawUrl, "page")) : 1;
            string strResult = "";
            int pageSize = 10;

            var listTour = (from t in db.Tours
                            join tp in db.TourPlaces on t.Id equals tp.IdTour
                            where lstCatInOutTop.Contains(t.IdPlace) && lstCatInOutTop.Contains(tp.IdPlace)
                            && t.Active == 1 && t.Lang == lang
                            select t).Distinct().ToList();

            if (plFrom != 0)
                listTour = listTour.Where(x => x.IdPlace == plFrom).ToList();

            if (plTo != 0)
            {
                List<int> lstTourId = db.TourPlaces.Where(x=>x.IdPlace == plTo).Select(x=>Convert.ToInt32(x.IdTour)).ToList();
                listTour = listTour.Where(x => lstTourId.Contains(x.Id)).ToList();
            }

            if (!string.IsNullOrEmpty(time) && !time.Equals("0"))
            {
                time = time.ToLower().Replace("ngày", "").Trim();
                if (time.Contains('>'))
                    listTour = listTour.Where(x => Convert.ToInt32(x.NumberOfDay) > 10).ToList();
                else
                    listTour = listTour.Where(x => Convert.ToInt32(x.NumberOfDay) == Convert.ToInt32(time)).ToList();
            }

            if (listTour.Any())
            {
                int count = listTour.Count;

                listTour = listTour.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                foreach (var item in listTour)
                {
                    strResult += "<div class='tourItems'>";
                    strResult += "  <div class='tourItems_info1'>";
                    strResult += "      <a class='tourItems_img' href='/" + item.Tagname + ".html' title='" + item.Name +
                                 "'><img src='" + item.Image1 + "' alt='" + item.Name + "' /></a>";
                    strResult += "      <a class='tourItems_link' href='/" + item.Tagname + ".html' title='" + item.Name + "'>" + item.Name + "</a>";
                    strResult += "      <span class='tourItems_priceOrifin'><strong>Giá: </strong>" + String.Format("{0:0,0}", item.PriceOrigin) +
                                 " " + item.PriceUnit + "</span>";
                    strResult += "      <span class='tourItems_price'><strong>Khuyến mại: </strong>" + String.Format("{0:0,0}", item.Price) + " " +
                                 item.PriceUnit + "</span>";
                    strResult += "      <span class='tourItems_topic'><strong>Chủ đề: </strong>" + GetName(item.IdTourTopic.Value) + "</span>";
                    strResult += "      <span class='tourItems_cat'><strong>Nhóm tour: </strong>" + GetName(item.IdTourCategory.Value) + "</span>";
                    strResult += "      <span class='tourItems_des'>" + MyWeb.Common.StringClass.GetContent(item.Description, 150) + "</span>";
                    strResult += "  </div>";
                    strResult += "  <div class='tourItems_info2'>";
                    strResult += "      <div class='tourItems_placeStart'><span>Điểm đi: </span>" + GetName(item.IdPlace.Value) + "</div>";
                    strResult += "      <div class='tourItems_placeEnd'><span>Điểm đến: </span>" + GetListPlaceName(item.Id) + "</div>";
                    strResult += "  </div>";
                    strResult += "  <div class='tourItems_info3'>";
                    strResult += "      <span class='tourItems_start'><strong>Thời gian khởi hành: </strong>" + item.Start +
                                 "</span>";
                    strResult += "      <span class='tourItems_time'><strong>Số ngày: </strong>" + item.NumberOfDay + "</span>";
                    strResult += "      <span class='tourItems_tran'><strong>Phương tiện: </strong>" + item.Transportation +
                                 "</span>";
                    strResult += "  </div>";
                    strResult += "</div>";
                }

                ltrContent.Text = strResult;
                ltrPaging.Text = common.PopulatePager(count, currentPage, pageSize);
            }
        }

        protected string GetName(int plId)
        {
            var item = db.tbPages.FirstOrDefault(s => s.pagId == plId);

            return item != null ? item.pagName : "";
        }

        protected string GetListPlaceName(int tourId)
        {
            var listPl = db.TourPlaces.Where(s => s.IdTour == tourId).Select(s => s.IdPlace).ToList();
            var listPage = db.tbPages.Where(s => listPl.Contains(s.pagId)).ToList();
            string strResult = "";

            if (listPage.Any())
            {

                foreach (var item in listPage)
                {
                    strResult += strResult != "" ? "</br><span class='itemTours_placeItem'>" + item.pagName + "</span>" : "<span class='itemTours_placeItem'>" + item.pagName + "</span>";
                }
            }

            return strResult;
        }
    }
}