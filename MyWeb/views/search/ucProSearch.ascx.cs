﻿using MyWeb.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.search
{
    public partial class ucProSearch : System.Web.UI.UserControl
    {
        string lang = "vi";
        int currentPage = 1;
        int recordCount = 0;
        int pageSize = 10;
        string strKeyword = "";
        string catId = "";
        float fotPriceFrom = 0;
        float fotPriceTo = 0;
        string viewBy = GlobalClass.viewProducts9;
        string sortBy = GlobalClass.viewProducts10;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                currentPage = Request.RawUrl.Contains("page") ? int.Parse(common.GetParameterFromUrl(Request.RawUrl, "page")) : 1;
            }
            catch { }
            try { pageSize = int.Parse(GlobalClass.viewProducts1); }
            catch { }
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                //string t = Session["tk"].ToString();
                if (Session["txtSearch"] != null && Session["txtSearch"].ToString() != "")
                    strKeyword = Session["txtSearch"].ToString().TrimEnd().TrimStart();
                if (Session["group"] != null && Session["group"].ToString() != "")
                    catId = Session["group"].ToString().TrimEnd().TrimStart();
                BindData();
            }
        }
        void BindData()
        {
            try
            {
                viewBy = !String.IsNullOrEmpty(Request.QueryString["view"]) ? Request.QueryString["view"].ToString() : viewBy;
                sortBy = !String.IsNullOrEmpty(Request.QueryString["sort"]) ? Request.QueryString["sort"].ToString() : sortBy;
                IEnumerable<tbProduct> product = db.tbProducts.Where(u => u.proActive == 1);
                product = product.Where(s => StringClass.RemoveUnicode(s.proName.ToLower()).Contains(StringClass.RemoveUnicode(strKeyword.ToLower())) || StringClass.RemoveUnicode(s.proCode.ToLower()).Contains(StringClass.RemoveUnicode(strKeyword.ToLower())) && s.proActive == 1 &&//);
                    (float.Parse(s.proPrice) >= fotPriceFrom || fotPriceTo == 0) &&
                    (float.Parse(s.proPrice) <= fotPriceTo || fotPriceTo == 0));
                product = product.Where(s => s.proLang.Trim() == lang);
                if (sortBy == "name")
                    if (sortBy == "name")
                        product = product.OrderBy(s => s.proName).ThenByDescending(s => s.proId).ToList();
                    else if (sortBy == "namedesc")
                        product = product.OrderByDescending(s => s.proName).ThenByDescending(s => s.proId).ToList();
                    else if (sortBy == "price")
                        product = product.OrderBy(s => s.proPrice).ThenByDescending(s => s.proId).ToList();
                    else if (sortBy == "pricedesc")
                        product = product.OrderByDescending(s => s.proPrice).ThenByDescending(s => s.proId).ToList();
                    else
                        product = product.OrderByDescending(s => s.proDate).ThenByDescending(s => s.proId).ToList();
                recordCount = product.Count();
                product = product.Skip((currentPage - 1) * pageSize).Take(pageSize);
                ltrlistpro.Text = "<ul class=\"product-list\">" + common.LoadProductList(product, viewBy) + "</ul>";
                ltrPaging.Text = common.PopulatePager(recordCount, currentPage, pageSize);
            }
            catch
            {
                Response.Redirect("/");
            }
        }
    }
}