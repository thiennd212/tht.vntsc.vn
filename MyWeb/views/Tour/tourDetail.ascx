﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="tourDetail.ascx.cs" Inherits="MyWeb.views.Tour.tourDetail" %>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f7e9d12301308fa" async="async"></script>
<%--<%@ Register Src="~/views/pages/ucBookNow.ascx" TagPrefix="uc100" TagName="ucBookNow" %>--%>
<div id="tourDetail">
    <div class="tourInfo">
        <div class="tourInfo_left">
            <asp:Literal runat="server" ID="ltrInfoLeft"></asp:Literal>
        </div>
        <div class="tourInfo_right">
            <asp:Literal runat="server" ID="ltrInfoRight"></asp:Literal>
            <asp:HiddenField ID="hdTourName" runat="server" />
            <asp:HiddenField ID="hdTourId" runat="server" />
            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Đặt tour</button>
            <%--<uc100:ucBookNow runat="server" ID="ucBookNow" />--%>
        </div>
    </div>
    <div class="tourContent">
        <asp:Literal runat="server" ID="ltrContent"></asp:Literal>
    </div>
    <div class="div-addthis-news">
        <div class="addthis_native_toolbox"></div>
    </div>
    <%if (GlobalClass.commentFNews.Contains("1"))
      {%><div class="shares">
          <div class="fb-comments" data-href="<%=strUrlFace %>" data-colorscheme="<%=cf %>" data-width="<%=wf %>" data-numposts="<%=nf %>"></div>
      </div>
    <%} %>
    <div id="areaComment">        
        <asp:Literal runat="server" ID="ltrListComment"></asp:Literal>   
        <asp:Literal runat="server" ID="ltrPagingComment"></asp:Literal>
        <asp:Label runat="server" ID="lbAlter" CssClass="alterComment"></asp:Label>
        <div id="formComment">
            <div class="rows">
                <label>Tên:</label>
                <asp:TextBox runat="server" ID="txtCommentName" CssClass="inputComment"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="rfvCommentName" ControlToValidate="txtCommentName" SetFocusOnError="true" ForeColor="Red" ErrorMessage="(*) Yêu cầu nhập nội dung" ValidationGroup="sendComment" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="rows">
                <label>Email:</label>
                <asp:TextBox runat="server" ID="txtCommentEmail" CssClass="inputComment"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="rfvCommentEmail" ControlToValidate="txtCommentEmail" SetFocusOnError="true" ForeColor="Red" ErrorMessage="(*) Yêu cầu nhập nội dung" ValidationGroup="sendComment" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="rows">
                <label>Nội dung:</label>
                <asp:TextBox runat="server" ID="txtCommentContent" CssClass="inputComment" TextMode="MultiLine" Rows="5"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="rfvCommentContent" ControlToValidate="txtCommentContent" SetFocusOnError="true" ForeColor="Red" ErrorMessage="(*) Yêu cầu nhập nội dung" ValidationGroup="sendComment" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="rows">
                <asp:LinkButton runat="server" ID="lbtSendComment" OnClick="lbtSendComment_Click" ValidationGroup="sendComment">Gửi</asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="tourOther">
        <asp:Literal runat="server" ID="ltrOther"></asp:Literal>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="header">MỜI BẠN ĐIỀN THÔNG TIN</h2>
            </div>
            <div class="modal-body">
                <%--<uc100:ucBookNow runat="server" ID="ucBookNow" />--%>
                <div class="box-sub-book-now bookNowForm">
                    <%--<div class="header bookNowTitle">
        <div class="txt-name-sub"><%= MyWeb.Global.GetLangKey("book-now") %></div>
    </div>--%>
                    <%--<asp:UpdatePanel ID="udpBook" runat="server">--%>
                        <ContentTemplate>
                            <div class="box-body-sub bookNowContent">
                                <div class="row">
                                    <div class="form-group MessengerBox">
                                        <label class="control-label col-md-4"></label>
                                        <div class="col-md-8">
                                            <span class="view-messenger">
                                                <asp:Label ID="lblthongbao" runat="server"></asp:Label></span>
                                        </div>
                                    </div>
                                    <div class="form-group bookNowName clearfix">
                                        <label class="control-label col-md-4">
                                            Thông tin khách hàng(*):
                            <asp:RequiredFieldValidator ID="rfvname" runat="server" ControlToValidate="txtname" Display="Dynamic" ErrorMessage="" SetFocusOnError="True" ValidationGroup="bookNow1"></asp:RequiredFieldValidator></label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtname" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group bookNowEmail clearfix">
                                        <label class="control-label col-md-4">
                                            Email(*):
                            <asp:RegularExpressionValidator ID="revmail" runat="server" ControlToValidate="txtmail" ErrorMessage="" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True" Display="Dynamic" ValidationGroup="bookNow1"></asp:RegularExpressionValidator></label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtmail" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group bookNowPhone clearfix">
                                        <label class="control-label col-md-4">
                                            Số điện thoại(*):
                            <asp:RequiredFieldValidator ID="rfvDienthoai" runat="server" ControlToValidate="txtDienthoai" Display="Dynamic" ErrorMessage="" SetFocusOnError="True" ValidationGroup="bookNow1"></asp:RequiredFieldValidator></label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtDienthoai" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group bookNowDate clearfix">
                                        <label class="control-label col-md-4">Ngày khởi hành:</label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtThoiGian" runat="server" class="form-control" data-provide="datepicker"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group bookNowNOP clearfix">
                                        <label class="control-label col-md-4">Số người:</label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtSoNguoi" runat="server" class="form-control" onkeyup="valid(this,'quotes')" onblur="valid(this,'quotes')">1</asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group bookNowContent clearfix">
                                        <label class="control-label col-md-4">Yêu cầu thêm (nếu có):</label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtnoidung" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group bookNowBtn">
                                        <label class="control-label col-md-4"></label>
                                        <div class="col-md-8">
                                            <asp:LinkButton ID="btnSend" class="btn btn-primary btn-md" runat="server" ValidationGroup="bookNow1" OnClick="btnSend_Click">Gửi thông tin</asp:LinkButton>
                                            <asp:Button ID="btnHideForm" runat="server" class="hide" Text="Hide" OnClientClick="MyFunction()"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    <%--</asp:UpdatePanel>--%>
                </div>
            </div>
            <%--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>--%>
        </div>

    </div>
</div>
<!--End modal-->
<script type="text/javascript">
    $(document).ready(function () {

        var sync1 = $("#sync1");
        var sync2 = $("#sync2");

        sync1.owlCarousel({
            singleItem: true,
            slideSpeed: 1000,
            navigation: false,
            pagination: false,
            afterAction: syncPosition,
            responsiveRefreshRate: 200,
        });

        sync2.owlCarousel({
            items: 5,
            itemsDesktop: [1199, 5],
            itemsDesktopSmall: [979, 10],
            itemsTablet: [768, 8],
            itemsMobile: [479, 4],
            navigation: true,
            navigationText: ["«", "»"],
            rewindNav: false,
            scrollPerPage: false,
            slideSpeed: 1500,
            pagination: false,
            paginationNumbers: false,
            autoPlay: false,
            afterInit: function (el) {
                el.find(".owl-item").eq(0).addClass("synced");
            }
        });

        var r = {
            'special': /[\W]/g,
            'quotes': /[^0-9^]/g,
            'notnumbers': /[^a-zA]/g
        }
        function valid(o, w) {
            o.value = o.value.replace(r[w], '');
        }

        function MyFunction() {
            $("#myModal").css("display", "none");
        }

        function syncPosition(el) {
            var current = this.currentItem;
            $("#sync2")
              .find(".owl-item")
              .removeClass("synced")
              .eq(current)
              .addClass("synced")
            if ($("#sync2").data("owlCarousel") !== undefined) {
                center(current)
            }
        }

        $("#sync2").on("click", ".owl-item", function (e) {
            e.preventDefault();
            var number = $(this).data("owlItem");
            sync1.trigger("owl.goTo", number);
        });

        function center(number) {
            var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
            var num = number;
            var found = false;
            for (var i in sync2visible) {
                if (num === sync2visible[i]) {
                    var found = true;
                }
            }

            if (found === false) {
                if (num > sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                } else {
                    if (num - 1 === -1) {
                        num = 0;
                    }
                    sync2.trigger("owl.goTo", num);
                }
            } else if (num === sync2visible[sync2visible.length - 1]) {
                sync2.trigger("owl.goTo", sync2visible[1])
            } else if (num === sync2visible[0]) {
                sync2.trigger("owl.goTo", num - 1)
            }

        }

    });
</script>

<script type="text/javascript">
    CloudZoom.quickStart();
    $('#txtThoiGian').datepicker({
        todayBtn: "linked",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        startDate: '-3d',
    });
</script>
