﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucLanguage : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnVietnam_Click(object sender, EventArgs e)
        {
            Session["Lang"] = "vi";
            MyWeb.Global.LoadConfig(Session["Lang"].ToString());
            Response.Redirect("/");
        }

        protected void btnEnglish_Click(object sender, EventArgs e)
        {
            Session["Lang"] = "en";
            MyWeb.Global.LoadConfig(Session["Lang"].ToString());
            Response.Redirect("/");
        }
    }
}