﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucSearchBoxV2 : System.Web.UI.UserControl
    {
        private string inputValue = MyWeb.Global.GetLangKey("search_text");
        private string lang = "vi";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            txtSearch.Attributes.Add("placeholder", inputValue);
            this.Page.Form.DefaultButton = btnSearch.UniqueID;
            if (Session["group"] != null)
                drlChuyenMuc.SelectedValue = Session["group"].ToString();
            else if (Request["hp"] != null)
            {
                string tagCat = Request["hp"].ToString();
                if (Request["hp"].Contains("/"))
                {
                    string[] arrRes = Request["hp"].ToString().Split('/');
                    tagCat = arrRes[0];
                    var pag = db.tbPages.Where(s => s.pagType == int.Parse(pageType.GP) && s.pagTagName == tagCat).FirstOrDefault();
                    if (pag != null)
                        drlChuyenMuc.SelectedValue = pag.pagId.ToString();
                }                
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            var strSearch = txtSearch.Text;
            Session["txtSearch"] = txtSearch.Text;
            Session["group"] = drlChuyenMuc.SelectedValue;
            Response.Redirect("/tim-kiem.html");
        }
    }
}