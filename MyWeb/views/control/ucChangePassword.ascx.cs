﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucChangePassword : System.Web.UI.UserControl
    {
        string strUser = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["um"] != null)
            {
                strUser = Session["um"].ToString();
            }
            else
            {
                Response.Redirect("/");
            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            string pass = vmmsclass.Encodingvmms.Encode(txtpassc.Text);
            string passn = vmmsclass.Encodingvmms.Encode(txtpass.Text);
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            string sql = "SELECT * FROM tbMembers WHERE uID='" + strUser + "' AND uPas='" + pass + "'";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (!IsEmpty(ds))
            {
                string sqlu = "UPDATE tbMembers SET uPas='" + passn + "' WHERE ID=" + ds.Tables[0].Rows[0]["ID"];
                conn.Open();
                conn.CreateCommand();
                SqlCommand cmd = new SqlCommand(sqlu, conn);
                int i = cmd.ExecuteNonQuery();
                conn.Close();
                conn.Dispose();
                if (i > 0)
                {
                    Response.Write("<script>alert('" + MyWeb.Global.GetLangKey("user_change_password_success") + "'); window.location.assign('http://" + Request.Url.Host + "')</script>");
                    txtpass.Text = "";
                    txtpass_confirm.Text = "";
                    txtpassc.Text = "";
                }
                else
                {
                    Response.Write("<script>alert('Error'); window.location.assign('http://" + Request.Url.Host + "/doi-mat-khau.aspx')</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('" + MyWeb.Global.GetLangKey("user_change_password_error") + "'); window.location.assign('http://" + Request.Url.Host + "/doi-mat-khau.aspx')</script>");
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtpass.Text = "";
            txtpass_confirm.Text = "";
            txtpassc.Text = "";
            Response.Redirect("/doi-mat-khau.html");
        }

        protected bool IsEmpty(DataSet dataSet)
        {
            foreach (DataTable table in dataSet.Tables)
                if (table.Rows.Count != 0) return false;

            return true;
        }

        protected void cvdpasrewrite_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (txtpass.Text == txtpass_confirm.Text) args.IsValid = true;
            else args.IsValid = false;
        }
    }
}