﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucFooter : System.Web.UI.UserControl
    {
        string lang = "vi";
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                BindData();
            }
        }
        private void BindData()
        {
            ltrFooter.Text = GlobalClass.conCopyright;
        }
    }
}