﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucRegister.ascx.cs" Inherits="MyWeb.views.control.ucRegister" %>
<div class="box-register">
    <div class="cate-header">
        <div class="txt-name-sub"><%= MyWeb.Global.GetLangKey("user_register") %></div>
    </div>
    <div class="box-body-sub">
        <div class="row">
            <div class="info"><asp:Label ID="lblConfig" runat="server"></asp:Label></div>
        </div>
        <div class="row">
            <div class="form-group">
                <span class="view-messenger"><asp:Label ID="lblthongbao" runat="server"></asp:Label></span>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("user_name")%>*:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtHoten" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2"><asp:RequiredFieldValidator ID="rfvname" runat="server" ControlToValidate="txtHoten" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True"></asp:RequiredFieldValidator></div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("user_username")%>*:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtUser" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2"><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUser" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True"></asp:RequiredFieldValidator></div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Email*:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtmail" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2"><asp:RegularExpressionValidator ID="revmail" runat="server" ControlToValidate="txtmail" ErrorMessage="Nhập Email đúng định dạng" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True" Display="Dynamic"></asp:RegularExpressionValidator></div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("user_password")%>*:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtpass" runat="server" class="form-control" TextMode="Password" onblur="if(this.value=='')this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue)this.value='';"></asp:TextBox>
                </div>
                <div class="col-md-2"><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtpass" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True"></asp:RequiredFieldValidator></div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("user_re_enter_password")%>*:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtpass_confirm" runat="server" class="form-control" TextMode="Password" onblur="if(this.value=='')this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue)this.value='';"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtpass_confirm" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtpass" ControlToValidate="txtpass_confirm" ErrorMessage="Mật khẩu phải trùng nhau!"></asp:CompareValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("contact_address")%>*:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtdiachi" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2"></div>
            </div>
             <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("contact_mobilephone")%>*:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtDienthoai" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2"></div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"></label>
                <div class="col-md-7">
                    <asp:LinkButton ID="btnSend" runat="server" OnClick="btnSend_Click"><%= MyWeb.Global.GetLangKey("user_register") %></asp:LinkButton>
                    <asp:LinkButton ID="btnReset" runat="server" OnClick="btnReset_Click"><%= MyWeb.Global.GetLangKey("contact_cancel")%></asp:LinkButton>
                </div>
            </div>

        </div>
    </div>
</div>
