﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucError404.ascx.cs" Inherits="MyWeb.views.control.ucError404" %>
<div class="error">
    <div class="error-code m-b-10">404 <i class="fa fa-warning"></i></div>
    <div class="error-content">
        <div class="error-message">Chúng tôi không thể tìm thấy nó...</div>
        <div class="error-desc m-b-20">
            Trang bạn đang tìm kiếm không tồn tại.<br />
        </div>
        <div>
            <a href="/" class="btn btn-success">Trở lại trang chủ</a>
        </div>
    </div>
</div>