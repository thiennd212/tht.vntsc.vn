﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucAdvright.ascx.cs" Inherits="MyWeb.views.control.ucAdvright" %>

<%if (showAdv)
  {%>
    <div class="box-adv-right">
        <div class="header"><%= MyWeb.Global.GetLangKey("advertise")%></div>
        <ul class="body-adv">
            <asp:Literal ID="ltradv" runat="server"></asp:Literal>
        </ul>
    </div>
<%} %>