﻿using MyWeb.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace MyWeb.views.products
{
    public partial class ucFilterManufac : System.Web.UI.UserControl
    {
        private string lang = "";
        public string catId = "";
        string strCatID = "";
        string manID = "";
        string tagCat = "";
        string tagMan = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = Global.GetLang();
            if (Request["cat"] != null)
            {
                try { catId = Request["cat"].ToString(); }
                catch { }
            }

            try
            {
                if (Request["hp"] != null)
                {
                    tagCat = Request["hp"].ToString();
                    if (Request["hp"].Contains("/"))
                    {
                        string[] arrRes = Request["hp"].ToString().Split('/');
                        tagCat = arrRes[0];
                        tagMan = arrRes[1];
                        manID = db.tbPages.Where(s => s.pagType == int.Parse(pageType.GM) && s.pagTagName == tagMan).FirstOrDefault().pagId.ToString();
                    }
                    strCatID = db.tbPages.Where(s => s.pagType == int.Parse(pageType.GP) && s.pagTagName == tagCat).FirstOrDefault().pagId.ToString();
                }
                string iCat = strCatID;
            }
            catch { }

            if (!IsPostBack)
            {
                BindData();
            }
        }
        private void BindData()
        {
            StringBuilder str = new StringBuilder();
            string strKeyword = "";
            float fotPriceFrom = 0;
            float fotPriceTo = 0;
            string host = ConfigurationManager.AppSettings["url-web-base"];
            if (strCatID != "" || (Session["txtSearch"] != null && Session["txtSearch"].ToString() != ""))
            {
                List<tbProduct> listpro = new List<tbProduct>();
                if (!string.IsNullOrEmpty(strCatID))
                {
                    tbPage category = db.tbPages.FirstOrDefault(s => s.pagId == int.Parse(strCatID));
                    if (category.paglevel.Length > 0)
                    {
                        var pagelistid = db.tbPages.Where(u => u.paglevel.StartsWith(category.paglevel) && u.pagType == int.Parse(pageType.GP) && u.pagActive == 1 && u.pagLang == lang).ToList();
                        var arrCatId = pagelistid.Select(s => s.pagId).ToArray();
                        listpro = db.tbProducts.Where(s => s.proActive == 1 && s.proLang == lang && arrCatId.Contains(Convert.ToInt32(s.catId))).ToList();
                    }
                }
                else if (Session["txtSearch"] != null && Session["txtSearch"].ToString() != "")
                {
                    /*strKeyword = Session["txtSearch"].ToString().TrimEnd().TrimStart();
                    listpro = db.tbProducts.Where(s => s.proActive == 1 && s.proLang == lang).ToList();
                    listpro = listpro.Where(s => StringClass.RemoveUnicode(s.proName.ToLower()).Contains(StringClass.RemoveUnicode(strKeyword.ToLower())) || StringClass.RemoveUnicode(s.proCode.ToLower()).Contains(StringClass.RemoveUnicode(strKeyword.ToLower())) && s.proActive == 1 &&//);
                    (float.Parse(s.proPrice) >= fotPriceFrom || fotPriceTo == 0) &&
                    (float.Parse(s.proPrice) <= fotPriceTo || fotPriceTo == 0)).ToList();*/

                    listpro = new List<tbProduct>();
                }

                string[] _strManID = listpro.Select(s => s.manufacturerId.ToString()).ToArray();

                List<tbPage> lstMan = (from a in db.tbPages
                                       where _strManID.Contains(a.pagId.ToString())
                                       select a).ToList();
                lstMan = lstMan.Distinct().ToList();
                if (lstMan != null && lstMan.Count() > 0)
                {
                    str.Append("<div class=\"header\">Hãng sản xuất</div><ul class=\"box-body-fillter-man\">");
                    foreach (var item2 in lstMan)
                    {
                        string crrTagMan = MyWeb.Common.StringClass.NameToTag(item2.pagTagName);
                        string href = host + "/" + tagCat + "/" + crrTagMan + ".html";
                        if (item2.pagImage.Length > 0)
                        {
                            if (crrTagMan == tagMan)
                                str.Append("<li class=\"man_type2_chk\" name=\"" + href + "\"><input type=\"checkbox\" checked=\"checked\" name=\"checksubman\" value=\"" + item2.paglevel + "\" class=\"man_type2_chkbox box" + item2.paglevel + "\" /><img src=\"" + item2.pagImage + "\" width=\"12px\" height=\"12px\" style=\"margin: 1px 4px 0 0px;\"/>" + item2.pagName + "</li>");
                            else
                                str.Append("<li class=\"man_type2_chk\" name=\"" + href + "\"><input type=\"checkbox\" name=\"checksubman\" value=\"" + item2.paglevel + "\" class=\"man_type2_chkbox box" + item2.paglevel + "\" /><img src=\"" + item2.pagImage + "\" width=\"12px\" height=\"12px\" style=\"margin: 1px 4px 0 0px;\"/>" + item2.pagName + "</li>");
                        }
                        else
                        {
                            if (crrTagMan == tagMan)
                                str.Append("<li class=\"man_type2_chk\" name=\"" + href + "\"><input type=\"checkbox\" checked=\"checked\" name=\"checksubman\" value=\"" + item2.paglevel + "\" class=\"man_type2_chkbox box" + item2.paglevel + "\" />" + item2.pagName + "</li>");
                            else
                                str.Append("<li class=\"man_type2_chk\" name=\"" + href + "\"><input type=\"checkbox\" name=\"checksubman\" value=\"" + item2.paglevel + "\" class=\"man_type2_chkbox box" + item2.paglevel + "\" />" + item2.pagName + "</li>");
                        }
                    }
                    str.Append("</ul>");
                }

                ltrData.Text = str.ToString();
            }
        }
    }
}