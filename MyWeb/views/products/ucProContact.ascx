﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProContact.ascx.cs" Inherits="MyWeb.views.products.ucProContact" %>
<div class="box-sub-prj-register prjRegisterForm">
            <div class="box-body-sub prjRegisterContent">
                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-md-4"></label>
                        <div class="col-md-8">
                            <span class="view-messenger">
                                <span id="thongbaopro"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group prjRegisterName clearfix">
                        <label class="control-label col-md-4">
                            Họ tên(*):                           
                        </label>
                        <div class="col-md-8">
                            <asp:TextBox ID="txtHoTenPro" runat="server" class="form-control" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group prjRegisterProject clearfix">
                        <label class="control-label col-md-4">Chọn xe:</label>
                        <div class="col-md-8">
                            <asp:CheckBoxList ID="chkListDongXe" runat="server" ClientIDMode="Static" RepeatColumns="2"></asp:CheckBoxList>
                            <asp:HiddenField ID="hdProCatName" runat="server" ClientIDMode="Static"/>
                        </div>
                    </div>
                    <div class="form-group prjRegisterProject clearfix">
                        <label class="control-label col-md-4">Hình thức:</label>
                        <div class="col-md-8">
                            <asp:RadioButtonList ID="rdListHinhThuc" runat="server" ClientIDMode="Static" RepeatColumns="2">
                                <asp:ListItem Value="Trả góp" Text="Trả góp" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="Trả hết" Text="Trả hết"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:HiddenField ID="hdHinhThuc" runat="server" ClientIDMode="Static" Value="Trả góp"/>
                        </div>
                    </div>
                    <div class="form-group prjRegisterEmail clearfix">
                        <label class="control-label col-md-4">
                            Địa chỉ(*):        
                        </label>
                        <div class="col-md-8">
                            <asp:TextBox ID="txtDiaChiPro" runat="server" class="form-control" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group prjRegisterEmail clearfix">
                        <label class="control-label col-md-4">
                            Email(*):        
                        </label>
                        <div class="col-md-8">
                            <asp:TextBox ID="txtEmailPro" runat="server" class="form-control" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group prjRegisterPhone clearfix">
                        <label class="control-label col-md-4">
                            Số điện thoại(*):  
                        </label>
                        <div class="col-md-8">
                            <asp:TextBox ID="txtSDTPro" runat="server" class="form-control" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>                    
                    <div class="form-group prjRegisterBtn">
                        <label class="control-label col-md-4"></label>
                        <div class="col-md-8">
                            <input type="button" class="btn btn-primary btn-md" onclick="SendDataPro()" value="Gửi thông tin" id="btSendPro" />
                        </div>
                    </div>
                </div>
            </div>
</div>
<script>
    function MyFunction() {
        $("#myModal").modal("hide");
    }
    var r = {
        'special': /[\W]/g,
        'quotes': /[^0-9^]/g,
        'notnumbers': /[^a-zA]/g
    }
    function valid(o, w) {
        o.value = o.value.replace(r[w], '');
    }

    $('#txtThoiGian').datepicker({
        todayBtn: "linked",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        startDate: '-3d',
    });

    $("#chkListDongXe input[type=checkbox]").on("click", function () {
        var val = $(this).val();
        var valHd = $("#hdProCatName").val();
        var arrValue = [];
        if (valHd != "")
            arrValue = valHd.split(",");
        if ($(this).prop("checked")) {
            arrValue.push(val);
        } else {
            var index = arrValue.indexOf(val);
            if (index > -1) {
                arrValue.splice(index, 1);
            }
        }
        if (arrValue.length > 0)
            $("#hdProCatName").val(arrValue.join());
        else
            $("#hdProCatName").val("");
    });
    
    $("#rdListHinhThuc input[type=radio]").on("click", function () {
        $("#hdHinhThuc").val($(this).val());
    });

    function SendDataPro() {
        $("#btSendPro").attr("disabled", "disabled");
        var hoten = $("#txtHoTenPro").val();
        var dongxe = $("#hdProCatName").val();
        var hinhthuc = $("#hdHinhThuc").val();
        var diachi = $("#txtDiaChiPro").val();
        var email = $("#txtEmailPro").val();
        var phone = $("#txtSDTPro").val();
        if (hoten == "" || dongxe == "" || email == "")
        {
            alert("Chưa nhập Họ tên, Chọn xe hoặc Email");
            return false;
        }
        $.ajax({
            async: false,
            type: "POST",
            url: "webService.asmx/SendProContact",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ hoten: hoten, dongxe: dongxe, hinhthuc: hinhthuc, diachi: diachi, email: email, phone: phone }),
            success: function (response) {
                console.log(response.d);
                if (response.d == true) {
                    $("#chkListDongXe input[type=checkbox]").each(function () {
                        $(this).prop('checked', false);
                    });
                    $("#txtHoTenPro").val("");
                    $("#hdProCatName").val("");                    
                    $("#txtDiaChiPro").val("");
                    $("#txtEmailPro").val("");
                    $("#txtSDTPro").val("");
                    $("#btSendPro").removeAttr("disabled");
                    $("span#thongbaopro").html("Bạn đã gửi thông tin thành công !!");
                } else {
                    $("#btSendPro").removeAttr("disabled");
                    $("span#thongbaopro").html("Lỗi không gửi được liên hệ !!");
                }
            },
            failure: function (response) {
                $("#btSendPro").removeAttr("disabled");
                $("span#thongbaopro").html("Lỗi không gửi được liên hệ !!");
            }

        });
    }
</script>
