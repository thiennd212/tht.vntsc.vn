﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProHotV2.ascx.cs" Inherits="MyWeb.views.products.ucProHotV2" %>
<%if (showAdv)
  {%>
<div class="box-product-hot">
    <div class="header">
        <%=MyWeb.Global.GetLangKey("pro_sl_nb") %>
    </div>
    <ul class="product-list">
        <asp:Literal ID="ltrProduct" runat="server"></asp:Literal>
    </ul>
</div>
<%} %>