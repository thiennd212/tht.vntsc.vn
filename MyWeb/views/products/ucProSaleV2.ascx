﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProSaleV2.ascx.cs" Inherits="MyWeb.views.products.ucProSaleV2" %>
<%if (showAdv)
  {%>
<div class="box-pro-sale">
    <div class="header"><%= MyWeb.Global.GetLangKey("pro_best_sale")%></div>
    <ul class="product-list">        
        <asp:Literal ID="ltrProduct" runat="server"></asp:Literal>        
    </ul>
</div>
<%} %>
