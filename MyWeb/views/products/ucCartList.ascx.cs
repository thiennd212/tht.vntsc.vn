﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.products
{
    public partial class ucCartList : System.Web.UI.UserControl
    {
        DataTable cartdt = new DataTable();
        string lang = "vi";
        string strProID = "";
        string strSL = "0";
        string strCatID = "";
        public string strPrice = "0";
        public string strError = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            strError = MyWeb.Global.GetLangKey("tt_error");
            if (Request["catid"] != null) { strCatID = Request["catid"].ToString(); }
            if (Request["sl"] != null) { strSL = Request["sl"].ToString(); }
            if (Request["proid"] != null) { strProID = Request["proid"].ToString(); }
            if (Session["prcart"] != null) { cartdt = (DataTable)Session["prcart"]; } else { Session["prcart"] = tbShopingcartDB.Cart_CreateCartDetail(); cartdt = (DataTable)Session["prcart"]; }

            if (GlobalClass.chkLogin == "1")
            {
                if (Session["uidr"] != null)
                {
                    ltrThanhToan.Text = "<a href=\"/thanh-toan.html\" title=\"" + MyWeb.Global.GetLangKey("button-order") + "\"><span>" + MyWeb.Global.GetLangKey("button-order") + "</span></a>";
                }
                else
                {
                    ltrThanhToan.Text = "<a href=\"javascript:{}\" onclick=\"clickOrder()\" title=\"" + MyWeb.Global.GetLangKey("button-order") + "\"><span>" + MyWeb.Global.GetLangKey("button-order") + "</span></a>";
                }
            }
            else
            {
                ltrThanhToan.Text = "<a href=\"/thanh-toan.html\" title=\"" + MyWeb.Global.GetLangKey("button-order") + "\"><span>" + MyWeb.Global.GetLangKey("button-order") + "</span></a>";
            }

            if (!IsPostBack)
            {
                AddiTemCart();
                BindData();
            }
        }
        void AddiTemCart()
        {
            if (Request["proid"] != null && Request["sl"] != null)
            {
                List<tbProductDATA> objList = tbProductDB.tbProduct_GetByTagName(strProID, lang);
                if (objList.Count > 0)
                {
                    string strStatus = objList[0].proStatus.ToString();
                    if (strStatus.Contains("0"))
                    {
                        if (objList[0].proPrice != "")
                        {
                            strPrice = objList[0].proPrice;
                        }
                        tbShopingcartDB.Cart_AddProduct(ref cartdt, objList[0].proId.ToString(), "0", objList[0].proName, strPrice, strSL, "1", objList[0].proImage.Split(Convert.ToChar(","))[0]);
                        objList.Clear();
                        objList = null;
                        Response.Redirect("/gio-hang.html");
                    }
                    else
                    {
                        Response.Write("<script type=\"text/javascript\">alert(\"Sản phẩm này đã hết bạn vui lòng đặt mua sản phẩm khác!\");window.history.back(-1);</script>﻿");

                    }
                }
            }
        }
        void BindData()
        {
            if (cartdt.Rows.Count > 0)
            {
                rptDanhsach.DataSource = cartdt;
                rptDanhsach.DataBind();
                Double Tongtien = 0;
                for (int i = 0; i < cartdt.Rows.Count; i++)
                {
                    Tongtien = Tongtien + Convert.ToDouble(cartdt.Rows[i]["money"].ToString());
                }
                lblTongtien.Text = String.Format("{0:N0}", Tongtien) + " " + GlobalClass.CurrencySymbol;
                Session["prcart"] = cartdt;
            }
            else
            {
                rptDanhsach.DataSource = cartdt;
                rptDanhsach.DataBind();
                lblTongtien.Text = "";
            }
            for (int i = 0; i < cartdt.Rows.Count; i++)
            {
                Label stt = (Label)rptDanhsach.Items[i].FindControl("lblstt");
                stt.Text = (i + 1).ToString();
            }
        }

        protected void rptDanhsach_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            DataTable dt = new DataTable();
            string strName = e.CommandName.ToString();
            strProID = e.CommandArgument.ToString();
            string surl = Request.Url.ToString();
            int index = e.Item.ItemIndex;
            switch (strName)
            {
                case "Update":
                    TextBox txtSL = (TextBox)rptDanhsach.Items[index].FindControl("txtSL");
                    if (txtSL.Text != "" && txtSL.Text != "0")
                    {
                        List<tbProductDATA> list = tbProductDB.tbProduct_GetByID(strProID);
                        if (list.Count > 0)
                        {
                            tbShopingcartDB.Cart_UpdateNumber(ref cartdt, list[0].proId.ToString(), txtSL.Text);
                        }
                        BindData();
                        Response.Redirect("/gio-hang.html");
                    }
                    break;
                case "Delete":
                    dt = cartdt;
                    List<tbProductDATA> list2 = tbProductDB.tbProduct_GetByID(strProID);
                    tbShopingcartDB.Cart_DeleteProduct(ref dt, list2[0].proId);
                    cartdt = dt;
                    Session["prcart"] = cartdt;
                    BindData();
                    Response.Redirect("/gio-hang.html");
                    break;
            }
        }
    }
}