﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb
{
    public partial class setsitemap : System.Web.UI.Page
    {
        string lang = "vi";
        string changeFreq = "weekly";
        string pagePriority = "0.80";
        string productPriority = "0.80";
        string newsPriority = "0.70";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ContentType = "text/xml";
            string text = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + Environment.NewLine;
            text += "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">" + Environment.NewLine;

            //Home page
            text += "<url>" + Environment.NewLine + "\t<loc>http://" + Request.Url.Host + "</loc>" + Environment.NewLine + "\t<changefreq>" + changeFreq + "</changefreq>" + Environment.NewLine + "\t<priority>1.00</priority>" + Environment.NewLine + "</url>" + Environment.NewLine;

            //Product
            IEnumerable<tbProduct> dbpro = db.tbProducts.Where(s => s.proLang == lang && s.proActive == 1).OrderByDescending(s => s.proId).OrderByDescending(s => s.proId);
            foreach (tbProduct p in dbpro)
            {
                string catId = p.catId.Split(Convert.ToChar(","))[0];
                if (MyWeb.Common.StringClass.Check(catId))
                    text += "<url>" + Environment.NewLine + "\t<loc>http://" + Request.Url.Host + "/" + p.proTagName + ".html</loc>" + Environment.NewLine + "\t<changefreq>" + changeFreq + "</changefreq>" + Environment.NewLine + "\t<priority>" + productPriority + "</priority>" + Environment.NewLine + "</url>" + Environment.NewLine;
            }

            //News
            IEnumerable<tbNew> dbnew = db.tbNews.Where(s => s.newLang == lang && s.newActive == 0).OrderByDescending(s => s.newid);
            foreach (tbNew n in dbnew)
            {
                text += "<url>" + Environment.NewLine + "\t<loc>http://" + Request.Url.Host + "/" + n.newTagName + ".html</loc>" + Environment.NewLine + "\t<changefreq>" + changeFreq + "</changefreq>" + Environment.NewLine + "\t<priority>" + newsPriority + "</priority>" + Environment.NewLine + "</url>" + Environment.NewLine;
            }

            //Page
            IEnumerable<tbPage> page = db.tbPages.Where(s => s.pagType == 1 && s.pagLang == lang && s.pagActive == 1).OrderByDescending(s => s.pagId);
            foreach (tbPage p in page)
            {
                text += "<url>" + Environment.NewLine + "\t<loc>http://" + Request.Url.Host + "/" + p.pagTagName + ".html</loc>" + Environment.NewLine + "\t<changefreq>" + changeFreq + "</changefreq>" + Environment.NewLine + "\t<priority>" + pagePriority + "</priority>" + Environment.NewLine + "</url>" + Environment.NewLine;
            }
            //sản phẩm
            IEnumerable<tbPage> page1 = db.tbPages.Where(s => s.pagType == int.Parse(pageType.GP) && s.pagLang == lang && s.pagActive == 1).OrderByDescending(s => s.pagId);
            foreach (tbPage p in page1)
            {
                text += "<url>" + Environment.NewLine + "\t<loc>http://" + Request.Url.Host + "/" + p.pagTagName + ".html</loc>" + Environment.NewLine + "\t<changefreq>" + changeFreq + "</changefreq>" + Environment.NewLine + "\t<priority>" + pagePriority + "</priority>" + Environment.NewLine + "</url>" + Environment.NewLine;
            }
            //News
            IEnumerable<tbPage> page2 = db.tbPages.Where(s => s.pagType == int.Parse(pageType.GN) && s.pagLang == lang && s.pagActive == 1).OrderByDescending(s => s.pagId);
            foreach (tbPage p in page2)
            {
                text += "<url>" + Environment.NewLine + "\t<loc>http://" + Request.Url.Host + "/" + p.pagTagName + ".html</loc>" + Environment.NewLine + "\t<changefreq>" + changeFreq + "</changefreq>" + Environment.NewLine + "\t<priority>" + pagePriority + "</priority>" + Environment.NewLine + "</url>" + Environment.NewLine;
            }

            //Library
            IEnumerable<Library> library = db.Libraries.Where(s => s.Lang == lang && s.Active == 1).OrderByDescending(s => s.Id);
            foreach (Library l in library)
            {
                text += "<url>" + Environment.NewLine + "\t<loc>http://" + Request.Url.Host + "/" + l.TagName + ".html</loc>" + Environment.NewLine + "\t<changefreq>" + changeFreq + "</changefreq>" + Environment.NewLine + "\t<priority>" + pagePriority + "</priority>" + Environment.NewLine + "</url>" + Environment.NewLine;
            }

            text += "</urlset>";

            //Create sitemap.xml file
            string pathfile = Server.MapPath("sitemap.xml");
            try
            {
                FileStream fs = null;
                if (!File.Exists(pathfile))
                {
                    using (fs = File.Create(pathfile)) { }
                }
                using (StreamWriter sw = new StreamWriter(pathfile))
                {
                    sw.Write(text);
                }
            }
            catch
            {
                //Download sitemap
                WebClient req = new WebClient();
                HttpResponse response = HttpContext.Current.Response;
                response.Clear();
                response.ClearContent();
                response.ClearHeaders();
                response.Buffer = true;
                FileInfo file = new FileInfo(pathfile);
                response.AddHeader("Content-Disposition", "attachment;filename=\"" + file.Name + "\"");
                byte[] data = req.DownloadData(pathfile);
                response.BinaryWrite(data);
                response.End();
            }

            Response.Write(text);
        }
    }
}