﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MyWeb
{
    public partial class error404 : System.Web.UI.Page
    {
        string strUrl = "";
        private string lang = "";
        string strTagID = "";
        int intEmptyHome = 0;
        public bool showPoupup = true, showSlide = true;
        dataAccessDataContext db = new dataAccessDataContext();
        public tbConfig objConfig;
        protected void Page_Load(object sender, EventArgs e)
        {
                Response.Redirect("/error404.html");
        }
    }
}