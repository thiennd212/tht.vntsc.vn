﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.client
{
    public partial class listPromotion : System.Web.UI.UserControl
    {
        private string lang = "vi";
        int pageSize = 15;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (!IsPostBack)
            {
                BindData();
            }
        }

        void BindData()
        {
            List<Mail_khuyenmai> _listForder = db.Mail_khuyenmais.OrderBy(s => s.id).ToList();
            if (_listForder.Count() > 0)
            {
                recordCount = _listForder.Count();
                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion

                var result = _listForder.Skip(currentPage * pageSize).Take(pageSize);
                rptFolderList.DataSource = result;
                rptFolderList.DataBind();
                BindPaging();
            }
            else
            {
                rptFolderList.DataSource = null;
                rptFolderList.DataBind();
            }
        }

        #region Page

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        #endregion

        protected string Actives(string actives)
        {
            string str = "";
            if (actives == "1")
            {
                str = "Hiển thị";
            }
            else
            {
                str = "Ẩn";
            }
            return str;
        }

        public static string ShowActive(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }

        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Del":
                    var obj = db.Mail_khuyenmais.FirstOrDefault(s => s.id == int.Parse(strID));
                    db.Mail_khuyenmais.DeleteOnSubmit(obj);
                    db.SubmitChanges();
                    ltrErr.Text = "Xóa không thành công !";
                    BindData();
                    pnlErr.Visible = true;
                    break;
                case "Edit":
                    var objEdit = db.Mail_khuyenmais.FirstOrDefault(s => s.id == int.Parse(strID));
                    txtEmail.Text = objEdit.mail;
                    hidID.Value = objEdit.mail;
                    pnlErr.Visible = false;
                    ltrErr.Text = "";
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    break;
                case "Active":
                    var objActive = db.Mail_khuyenmais.FirstOrDefault(s => s.id == int.Parse(strID));
                    if (objActive.active == 0)
                    {
                        objActive.active = 1;
                    }
                    else { objActive.active = 0; }
                    db.SubmitChanges();
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Cập nhật trạng thái thành công !";
                    break;
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            hidID.Value = "";
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = false;
            pnlAddForder.Visible = true;
            hidID.Value = "ALL";
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            List<tbConfigDATA> objConfig = tbConfigDB.tbConfig_GetLang(lang);
            string mailfrom = objConfig[0].conMail_Noreply;
            string mailto = objConfig[0].conMail_Info;
            string pas = objConfig[0].conMail_Pass;
            string host = host = "smtp.gmail.com";
            if (objConfig[0].conMail_Method.Length > 0)
            {
                host = objConfig[0].conMail_Method;
            }
            int post = 465;
            if (objConfig[0].conMail_Port.Length > 0)
            {
                post = int.Parse(objConfig[0].conMail_Port);
            }
            string strContain = "";
            strContain = fckChitiet.Text;
            string strCc = "";
            if (hidID.Value == "ALL")
            {
                List<Mail_khuyenmai> lst = db.Mail_khuyenmais.Where(s => s.active == 1).OrderBy(s => s.id).ToList();
                foreach (var item in lst)
                {
                    strCc += item.mail + ",";
                }
            }
            try
            {
                common.SendMail(mailto, strCc, "", "", "Thông tin khuyến mại trên website " + Request.Url.Host, strContain);
                pnlListForder.Visible = true;
                pnlAddForder.Visible = false;
                BindData();
                hidID.Value = "";
                pnlErr.Visible = true;
                ltrErr.Text = "Gửi email thành công !";

            }
            catch
            {
            }
            

        }
    }
}