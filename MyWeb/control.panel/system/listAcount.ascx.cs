﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.system
{
    public partial class listAcount : System.Web.UI.UserControl
    {
        private string lang = "vi";
        int pageSize = 15;
        static string Level = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (!IsPostBack)
            {
                BindModules();
                BindData();
            }
        }
        void BindData()
        {
            List<tbUserDATA> objList = new List<tbUserDATA>();
            objList = tbUserDB.tbUser_GetByAll();
            if (objList.Count() > 0)
            {
                recordCount = objList.Count();
                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion

                var result = objList.Skip(currentPage * pageSize).Take(pageSize);
                rptFolderList.DataSource = result;
                rptFolderList.DataBind();
                BindPaging();
            }
            else
            {
                rptFolderList.DataSource = null;
                rptFolderList.DataBind();
            }
        }

        #region Page

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        #endregion

        public string BindName(string strLever, string strName)
        {
            string str = "";
            if (strLever.Length > 5)
            {
                for (int j = 0; j < strLever.Length / 5; j++)
                {
                    str += "-- ";
                }
                str += strName;
            }
            else
            {
                str += "<b>" + strName + "</b>";
            }

            return str;
        }

        void BindModules()
        {
            var objModules = db.tbModules.Where(s => s.active == true).OrderBy(s => s.treecode).ToList();
            if (objModules.Count() > 0)
            {
                rptModule.DataSource = objModules;
                rptModule.DataBind();

                for (int i = 0; i < objModules.Count; i++)
                {
                    Label lblorder = (Label)rptModule.Items[i].FindControl("lblorder");
                    int k = (i + 1);
                    lblorder.Text = k.ToString();
                }

            }
        }

        public static string ShowActive(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = false;
            pnlAddForder.Visible = true;
            Resetcontrol();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            RepeaterItem item = default(RepeaterItem);
            for (int i = 0; i < rptFolderList.Items.Count; i++)
            {
                item = rptFolderList.Items[i];
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    if (((CheckBox)item.FindControl("chkBox")).Checked)
                    {
                        HiddenField hidID = (HiddenField)item.FindControl("hidCatID");
                        var data = db.tbUsers.Where(u => u.useId == Convert.ToInt32(hidID.Value)).FirstOrDefault();
                        db.tbUsers.DeleteOnSubmit(data);
                        db.SubmitChanges();
                    }
                }
            }
            pnlErr.Visible = true;
            ltrErr.Text = "Xóa thành công !";
            BindData();
            chkSelectAll.Checked = false;
        }

        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "AutoLogin":
                    List<tbUserDATA> _list = tbUserDB.tbUser_GetByID(strID);
                    Session["ctrname"] = "";
                    Session["p"] = _list[0].useLevel.ToString();
                    Session["uid"] = _list[0].useId.ToString();
                    MyWeb.Common.GlobalClass.CurrentUserName = _list[0].useId.ToString();
                    Session["user"] = _list[0].useUid;
                    Session["hoten"] = _list[0].useName;
                    Session["admin"] = _list[0].useAdmin;

                    Session["IsAuthorized"] = true;
                    Session["UserRole"] = _list[0].useRole;
                    Session["strAuthorized"] = _list[0].useAuthorities;
                    //cookie.SetCookie("SessionCount", "0");
                    Response.Redirect("/control.panel/");
                    break;
                case "Del":
                    tbUserDB.tbUser_Delete(strID);
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Xóa thành công !";
                    break;
                case "Edit":
                    BindModules();
                    string[] _strAuthor;
                    hidID.Value = strID;
                    List<tbUserDATA> list = tbUserDB.tbUser_GetByID(strID);
                    txtUserID.Text = list[0].useUid;
                    txtTen.Text = list[0].useName;
                    Level = list[0].useLevel;
                    txtPassID.Text = vmmsclass.Encodingvmms.Decode(list[0].usePid);
                    txtEmail.Text = list[0].useMail;

                    if (list[0].useAdmin == "1")
                    {
                        chkQuantri.Checked = true;
                    }
                    else
                    {
                        chkQuantri.Checked = false;
                    }

                    try
                    {
                        ddlquantri.SelectedValue = list[0].useRole;

                        if (list[0].useRole == "1")
                        {
                            pnlSetPer.Visible = false;
                        }
                        else
                        {
                            pnlSetPer.Visible = true;
                        }
                    }
                    catch { pnlSetPer.Visible = false; }


                    if (list[0].useActive == "1")
                    {
                        chkKichhoat.Checked = true;
                    }
                    else
                    {
                        chkKichhoat.Checked = false;
                    }

                    //Load modules
                    if (list[0].useAuthorities != null)
                    {
                        try
                        {
                            _strAuthor = list[0].useAuthorities.Split(';').ToArray();
                            for (int i = 0; i < rptModule.Items.Count; i++)
                            {
                                HiddenField hidModuleID = (HiddenField)rptModule.Items[i].FindControl("hidModuleID");
                                if (_strAuthor.Contains(hidModuleID.Value))
                                {
                                    Label lblActive = (Label)rptModule.Items[i].FindControl("lblActive");
                                    Literal tbxAnswer = (Literal)rptModule.Items[i].FindControl("ltrIcon");
                                    lblActive.Text = "1";
                                    tbxAnswer.Text = "<img src=\"../../theme/admin_cms/img/iconcheck.png\" />";
                                }

                            }
                        }
                        catch { }
                    }

                    txtThuTu.Text = list[0].useOrd;
                    pnlErr.Visible = false;
                    ltrErr.Text = "";
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    break;
                case "Active":
                    tbUser us = db.tbUsers.Where(u => u.useId == Convert.ToInt32(strID)).FirstOrDefault();
                    if (us.useActive == 0)
                    {
                        us.useActive = 1;
                    }
                    else { us.useActive = 0; }
                    db.SubmitChanges();
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Cập nhật trạng thái thành công !";
                    break;
            }
        }

        protected void txtNumberOrder_TextChanged(object sender, EventArgs e)
        {
            TextBox txtThutuss = (TextBox)sender;
            var b = (Label)txtThutuss.FindControl("Label16");

            List<tbUserDATA> list = tbUserDB.tbUser_GetByID(b.Text);
            list[0].useOrd = txtThutuss.Text;
            tbUserDB.tbUser_Update(list[0]);
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật thứ tự thành công !";
        }

        protected string QuyenAdmin(string admin)
        {
            string str = "";
            if (admin == "1")
            {
                str = "Full quyền";
            }
            else if (admin == "0")
            {
                str = "Hạn chế";
            }
            else
            {
                str = "Toàn quyền";
            }
            return str;
        }

        protected string RoleAdmin(string admin)
        {
            string str = "";
            if (admin == "1")
            {
                str = "Tổng biên tập";
            }
            else if (admin == "2")
            {
                str = "Quản trị viên";
            }
            else
            {
                str = "Toàn quyền";
            }
            return str;
        }

        protected bool Validation()
        {
            if (txtUserID.Text == "") { ltrErr2.Text = "Chưa nhập Acount thành viên !!"; pnlErr2.Visible = true; return false; }
            if (txtPassID.Text == "") { ltrErr2.Text = "Chưa nhập mật khẩu thành viên !!"; pnlErr2.Visible = true; return false; }
            if (txtTen.Text == "") { ltrErr2.Text = "Chưa nhập tên thành viên !!"; pnlErr2.Visible = true; return false; }
            if (txtEmail.Text == "") { ltrErr2.Text = "Chưa nhập email !!"; pnlErr2.Visible = true; return false; }
            return true;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Validation() == true)
            {
                string strModule = "";
                string strActive = "";
                string strAdmin = "";
                if (chkKichhoat.Checked == true) { strActive = "1"; } else { strActive = "0"; }
                if (chkQuantri.Checked == true) { strAdmin = "1"; } else { strAdmin = "0"; }
                //Add module cho user
                if (ddlquantri.SelectedValue == "2")
                {
                    for (int i = 0; i < rptModule.Items.Count; i++)
                    {
                        HiddenField hidModuleID = (HiddenField)rptModule.Items[i].FindControl("hidModuleID");
                        Label lblActive = (Label)rptModule.Items[i].FindControl("lblActive");
                        if (lblActive.Text == "1")
                        {
                            strModule += hidModuleID.Value + ";";
                        }
                    }
                }
                else
                {
                    strModule = "";
                }

                string strID = hidID.Value;
                if (strID.Length == 0)
                {
                    tbUserDATA ObjtbUserDATA = new tbUserDATA();
                    ObjtbUserDATA.useUid = txtUserID.Text;
                    ObjtbUserDATA.usePid = vmmsclass.Encodingvmms.Encode(txtPassID.Text);
                    ObjtbUserDATA.useName = txtTen.Text;
                    ObjtbUserDATA.useRole = ddlquantri.SelectedValue;
                    ObjtbUserDATA.useAdmin = strAdmin;
                    ObjtbUserDATA.useAuthorities = strModule;
                    ObjtbUserDATA.useMail = txtEmail.Text;
                    if (Level.Length > 0)
                    {
                        ObjtbUserDATA.useLevel = Level + "00000";
                    }
                    else
                    {
                        ObjtbUserDATA.useLevel = Level;
                    }
                    ObjtbUserDATA.useOrd = txtThuTu.Text;
                    ObjtbUserDATA.useActive = strActive;
                    //ObjtbUserDATA.useRole = "0";
                    if (tbUserDB.tbUser_Add(ObjtbUserDATA))
                    {
                        pnlErr.Visible = true;
                        ltrErr.Text = "Thêm mới thành công !";
                        BindData();
                        BindModules();
                        Resetcontrol();
                        pnlListForder.Visible = true;
                        pnlAddForder.Visible = false;
                    }
                }
                else
                {
                    if (strID.Length > 0)
                    {
                        List<tbUserDATA> list = tbUserDB.tbUser_GetByID(strID);
                        if (list.Count > 0)
                        {
                            list[0].useId = strID;
                            list[0].useUid = txtUserID.Text;
                            if (txtPassID.Text != "")
                            {
                                list[0].usePid = vmmsclass.Encodingvmms.Encode(txtPassID.Text);
                            }
                            else
                            {
                                list[0].usePid = "";
                            }
                            list[0].useName = txtTen.Text;

                            list[0].useAdmin = strAdmin;
                            list[0].useRole = ddlquantri.SelectedValue; ;

                            list[0].useOrd = txtThuTu.Text;
                            list[0].useActive = strActive;
                            list[0].useAuthorities = strModule;
                            list[0].useMail = txtEmail.Text;
                            if (tbUserDB.tbUser_Update(list[0]))
                            {
                                pnlErr.Visible = true;
                                ltrErr.Text = "Cập nhật thành công !";
                                Resetcontrol();
                                BindData();
                                BindModules();
                                hidID.Value = "";
                                pnlListForder.Visible = true;
                                pnlAddForder.Visible = false;
                            }
                        }
                    }
                }
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            BindModules();
            Session["insert"] = "false";
            hidID.Value = "";
            pnlSetPer.Visible = false;
        }

        void Resetcontrol()
        {
            txtUserID.Text = "";
            txtPassID.Text = "";
            txtTen.Text = "";
            txtEmail.Text = "";
            chkQuantri.Checked = false;
            chkKichhoat.Checked = false;
            txtThuTu.Text = "1";
            hidID.Value = "";
        }

        protected void ddlquantri_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlquantri.SelectedValue == "1")
            {
                pnlSetPer.Visible = false;
            }
            else
            {
                pnlSetPer.Visible = true;
            }
        }

        protected void rptModule_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblActive = e.Item.FindControl("lblActive") as Label;
                Literal tbxAnswer = e.Item.FindControl("ltrIcon") as Literal;
                if (lblActive.Text == "0")
                {
                    tbxAnswer.Text = "<img src=\"../../theme/admin_cms/img/iconuncheck.png\" />";
                }
                else
                {
                    tbxAnswer.Text = "<img src=\"../../theme/admin_cms/img/iconcheck.png\" />";
                }
            }
        }

        protected void rptModule_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "ActiveUnActive":
                    Label lblActive = e.Item.FindControl("lblActive") as Label;
                    Literal tbxAnswer = e.Item.FindControl("ltrIcon") as Literal;


                    if (lblActive.Text == "0")
                    {
                        lblActive.Text = "1";
                    }
                    else
                    {
                        lblActive.Text = "0";
                    }


                    if (lblActive.Text == "0")
                    {
                        tbxAnswer.Text = "<img src=\"../../theme/admin_cms/img/iconuncheck.png\" />";
                    }
                    else
                    {
                        tbxAnswer.Text = "<img src=\"../../theme/admin_cms/img/iconcheck.png\" />";
                    }

                    // Kích hoạt cha

                    if (strID.Length > 5)
                    {
                        for (int i = 0; i < rptModule.Items.Count; i++)
                        {
                            HiddenField hidModuleID = (HiddenField)rptModule.Items[i].FindControl("hidModuleID");
                            if (hidModuleID.Value == strID.Substring(0, 5))
                            {
                                Label lblActiveT = (Label)rptModule.Items[i].FindControl("lblActive");
                                Literal tbxAnswerT = (Literal)rptModule.Items[i].FindControl("ltrIcon");
                                lblActiveT.Text = "1";
                                tbxAnswerT.Text = "<img src=\"../../theme/admin_cms/img/iconcheck.png\" />";
                            }

                        }
                    }
                    // Kích hoạt con
                    else
                    {
                        for (int i = 0; i < rptModule.Items.Count; i++)
                        {
                            HiddenField hidModuleID = (HiddenField)rptModule.Items[i].FindControl("hidModuleID");
                            if (hidModuleID.Value.StartsWith(strID) && hidModuleID.Value.Length > 5)
                            {
                                Label lblActiveT = (Label)rptModule.Items[i].FindControl("lblActive");
                                Literal tbxAnswerT = (Literal)rptModule.Items[i].FindControl("ltrIcon");
                                if (lblActive.Text == "1")
                                {
                                    lblActiveT.Text = "1";
                                    tbxAnswerT.Text = "<img src=\"../../theme/admin_cms/img/iconcheck.png\" />";
                                }
                                else
                                {
                                    lblActiveT.Text = "0";
                                    tbxAnswerT.Text = "<img src=\"../../theme/admin_cms/img/iconuncheck.png\" />";
                                }
                            }
                        }
                    }

                    break;
            }
        }

    }
}