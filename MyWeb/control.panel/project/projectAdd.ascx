﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="projectAdd.ascx.cs" Inherits="MyWeb.control.panel.project.projectAdd" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<script src="../../scripts/ckfinder/ckfinder.js" type="text/javascript"></script>
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="/control.panel/">Trang chủ</a></li>
        <li><a href="/admin-product/list.aspx">Dự án</a></li>
        <li class="active">Thêm mới dự án</li>
    </ol>
    <!-- end breadcrumb -->
    <h1 class="page-header">Thêm mới dự án</h1>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse panel-with-tabs" data-sortable-id="ui-unlimited-tabs-1">
                <div class="panel-heading p-0">
                    <div class="panel-heading-btn m-r-10 m-t-10">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    </div>
                    <div class="tab-overflow">
                        <ul class="nav nav-tabs nav-tabs-inverse">
                            <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-inverse"><i class="fa fa-arrow-left"></i></a></li>
                            <li class="active"><a href="#info-tab" data-toggle="tab">Thông tin dự án</a></li>
                            <li class=""><a href="#contain-tab" data-toggle="tab">Nội dung chi tiết</a></li>
                            <li class=""><a href="#img-tab" data-toggle="tab">Hình ảnh và video</a></li>
                            <li class=""><a href="#menu-tab" data-toggle="tab">Menu theo dự án</a></li>
                            <li class="hidden"><a href="#price-tab" data-toggle="tab">Thông tin giá, khuyến mại</a></li>
                            <li class=""><a href="#seo-tab" data-toggle="tab">Cấu hình SEO</a></li>
                            <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-inverse"><i class="fa fa-arrow-right"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content panel-body panel-form">
                    <asp:Panel ID="pnlErr" runat="server" Visible="false">
                        <div class="alert alert-danger fade in" style="border-radius: 0px;">
                            <button class="close" data-dismiss="alert" type="button">
                                <span aria-hidden="true">×</span>
                            </button>
                            <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                        </div>
                    </asp:Panel>
                    <%--TABS INFO--%>
                    <div class="tab-pane fade active in" id="info-tab">
                        <div class="form-horizontal form-bordered">
                            <div class="form-group">
                                <label class="control-label col-md-2">Nhóm dự án *:</label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="ddlForder" runat="server" class="form-control"></asp:DropDownList>
                                    <asp:HiddenField ID="hidIDprj" runat="server" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Tên dự án *:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtTen" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Mã dự án:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtMa" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Địa chỉ:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtproWarranty" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Mô tả ngắn:</label>
                                <div class="col-md-10">
                                    <CKEditor:CKEditorControl ID="fckTomtat" runat="server"></CKEditor:CKEditorControl>
                                </div>                            
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Nội dung:</label>
                                <div class="col-md-10">
                                    <CKEditor:CKEditorControl ID="fckTongQuan" runat="server"></CKEditor:CKEditorControl>
                                </div>                            
                            </div>

                            <div class="form-group hidden">
                                <label class="control-label col-md-2">Mặt bằng:</label>
                                <div class="col-md-10">
                                    <CKEditor:CKEditorControl ID="fckMatBang" runat="server" Toolbar="Basic"></CKEditor:CKEditorControl>
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="control-label col-md-2">Bản đồ vị trí:</label>
                                <div class="col-md-10">
                                    <CKEditor:CKEditorControl ID="fckBanDoViTri" runat="server" Toolbar="Basic"></CKEditor:CKEditorControl>
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="control-label col-md-2">Tiện ích:</label>
                                <div class="col-md-10">
                                    <CKEditor:CKEditorControl ID="fckTienIchHoanHao" runat="server" Toolbar="Basic"></CKEditor:CKEditorControl>
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="control-label col-md-2">Chương trình bán hàng:</label>
                                <div class="col-md-10">
                                    <CKEditor:CKEditorControl ID="fckChuongTrinhBanHang" runat="server" Toolbar="Basic"></CKEditor:CKEditorControl>
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="control-label col-md-2">Giá bán & thanh toán:</label>
                                <div class="col-md-10">
                                    <CKEditor:CKEditorControl ID="fckGiaBanVaThanhToan" runat="server" Toolbar="Basic"></CKEditor:CKEditorControl>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Giá bán:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtGia" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Ngày đăng:</label>
                                <div class="col-md-4">
                                    <div class="input-group date datepicker-autoClose">
                                        <asp:TextBox ID="txtNewsDate" runat="server" class="form-control datepicker-autoClose"></asp:TextBox>
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Tùy chọn:</label>
                                <div class="col-md-7">
                                    <asp:CheckBox runat="server" ID="chkNoibat" Text="Dự án nổi bật" />
                                    &nbsp;&nbsp;&nbsp;                                
                                    <asp:CheckBox ID="chkSpbanchay" runat="server" Text="Dự án sale" />
                                    &nbsp;&nbsp;&nbsp;
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Kích hoạt:</label>
                                <div class="col-md-7">
                                    <asp:CheckBox ID="chkKichhoat" runat="server" />
                                </div>
                            </div>

                        </div>
                    </div>

                    <%--TABS NỘI DUNG--%>
                    <div class="tab-pane fade" id="contain-tab">
                        <div class="form-horizontal form-bordered">
                            <asp:Literal runat="server" ID="ltrTabBind"></asp:Literal>
                            <div class="form-group">
                                <label class="control-label col-md-2"></label>
                                <div class="col-md-7">
                                    <input id="btnAddprj" type="button" value="Thêm tab" class="btn btn-primary btn-sm" />
                                </div>
                            </div>
                            <div style="display: none;">
                                <asp:TextBox runat="server" ID="ltrHidValue"></asp:TextBox>
                                <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdCountTab" />
                            </div>
                        </div>
                    </div>

                    <%--TABS ẢNH--%>
                    <div class="tab-pane fade" id="img-tab">
                        <div class="form-horizontal form-bordered">
                            <div class="form-group">
                                <label class="control-label col-md-2">Số lượng ảnh:</label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="ddlImage" runat="server" class="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div id="list_image">
                                <asp:Repeater ID="rptImages" runat="server">
                                    <ItemTemplate>
                                        <div class="form-group" id='row<%#Eval("Id") %>'>
                                            <label class="control-label col-md-2">Chọn ảnh:</label>
                                            <div class="col-md-7">
                                                <div class="input-group">
                                                    <div class="input-group-btn">
                                                        <asp:HiddenField ID="txtMId" runat="server" />
                                                        <asp:TextBox ID="txtMImage" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                        <button id="btnMImgImage" class="btn btn-success" type="button" style="margin-left: -12px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Mã nhúng youtube:</label>
                                <div class="col-md-5">
                                    <asp:TextBox ID="txtVideo" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <div class="col-md-5">
                                    <img src="../../theme/admin_cms/img/demoyou.jpg" width="270" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="menu-tab">
                        <div class="form-horizontal form-bordered">                            
                            <%if (menu)
                              { %>
                            <div class="form-group">
                                <label class="control-label col-md-2">Danh mục menu:</label>
                                <div class="col-md-7">
                                    <asp:CheckBoxList ID="chkMenu" runat="server"></asp:CheckBoxList>
                                </div>
                            </div>
                            <%} %>
                        </div>
                    </div>

                    <%--TABS GIÁ--%>
                    <div class="tab-pane fade" id="price-tab">
                        <div class="form-horizontal form-bordered">
                            <div class="form-group">
                                <label class="control-label col-md-2">Giá niêm yết:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtGiagoc" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Loại tiền:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtLoaitien" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group" id="trTextKM" runat="server">
                                <label class="control-label col-md-2">Nội dung khuyến mại:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtPromotions" runat="server" TextMode="MultiLine" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group" id="trThoigianKM" runat="server">
                                <label class="control-label col-md-2">Thời gian khuyến mại:</label>
                                <div class="col-md-3">
                                    <div class="input-group date datepicker-autoClose">
                                        <asp:TextBox ID="txtTungay" runat="server" class="form-control datepicker-autoClose"></asp:TextBox>
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                                <label class="control-label col-md-1">Đến ngày:</label>
                                <div class="col-md-3">
                                    <div class="input-group date datepicker-autoClose">
                                        <asp:TextBox ID="txtDenngay" runat="server" class="form-control datepicker-autoClose"></asp:TextBox>
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <%--TABS SEO--%>
                    <div class="tab-pane fade" id="seo-tab">
                        <div class="form-horizontal form-bordered">
                            <div class="form-group">
                                <label class="control-label col-md-2">Meta title:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtTieude" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Meta description:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtDesscription" runat="server" TextMode="MultiLine" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Meta keyword:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtKeyword" runat="server" TextMode="MultiLine" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="control-label col-md-2">Tags dự án:</label>
                                <div class="col-md-7">
                                    <a onclick="javascript:ShowBlogNewOther('divRelate')" style="font-size: 20px; cursor: pointer;"><i class="fa ion-ios-pricetags"></i></a>
                                </div>
                            </div>
                            <div id="divRelate" style="display: none;">
                                <asp:UpdatePanel ID="udpTags" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group">
                                            <label class="control-label col-md-2"></label>
                                            <div class="col-md-4">
                                                <asp:ListBox ID="lstNew" runat="server" Rows="10" class="form-control"></asp:ListBox>
                                            </div>
                                            <div class="col-md-1">
                                                <asp:Button class="btn btn-primary" ID="btnActive" runat="server" Text=">>" OnClick="btnActive_Click" />
                                                <asp:Button class="btn btn-primary" ID="btnUnactive" runat="server" Text="<<" OnClick="btnUnactive_Click" />
                                            </div>
                                            <div class="col-md-4">
                                                <asp:ListBox ID="lstB" runat="server" Visible="false" class="form-control"></asp:ListBox>

                                                <asp:ListBox ID="lstNewRelate" runat="server" Rows="10" class="form-control"></asp:ListBox>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal form-bordered">
                        <div class="form-group" style="border-top: 1px solid #eee;">
                            <label class="control-label col-md-2"></label>
                            <div class="col-md-7">
                                <asp:LinkButton ID="btnUpdateProject" runat="server" OnClick="btnUpdate_Click" class="btn btn-primary">Cập nhật</asp:LinkButton>
                                <asp:LinkButton ID="btnReset" runat="server" OnClick="btnReset_Click" class="btn btn-danger">Hủy</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>