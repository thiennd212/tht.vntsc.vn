﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucSlides.ascx.cs" Inherits="MyWeb.views.control.ucSlides" %>

<%if(showSlide){%>
<div class="slides">
    <div id="owl-demo" class="owl-carousel">
        <asp:Literal ID="ltrSlides" runat="server"></asp:Literal>
    </div>
</div>
<!--end slide-->
<%} %>