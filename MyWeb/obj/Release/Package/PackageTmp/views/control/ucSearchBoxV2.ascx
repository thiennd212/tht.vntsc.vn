﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucSearchBoxV2.ascx.cs" Inherits="MyWeb.views.control.ucSearchBoxV2" %>

<div class="box-search">
    <div class="col-md-12 padding-none">
        <div class="col-md-3 padding-right-none">
            <asp:DropDownList ID="drlChuyenMuc" runat="server" class="form-control"></asp:DropDownList>
        </div>
        <div class="col-md-7 padding-none">
            <asp:TextBox ID="txtSearch" runat="server" class="input-text" placeholder="Nhập từ khóa tìm kiếm"></asp:TextBox>
        </div>
        <div class="col-md-2">
            <asp:LinkButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" class="btn-search color-backgroud"><i class="fa fa-search"></i></asp:LinkButton>
        </div>
    </div>
</div>
