﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucFeedbackDetail.ascx.cs" Inherits="MyWeb.views.news.ucFeedbackDetail" %>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f7e9d12301308fa" async="async"></script>
  <div  class="box-feedback-detail">
    <div class="cate-header sub-top">
        <div class="txt-name-sub">
            <asp:Literal ID="ltrNewName" runat="server"></asp:Literal>
        </div>
    </div>
    
    <div class="body-detailt">
        <asp:Repeater ID="rptNewsDetails" runat="server">
            <ItemTemplate>
                <div class="news-img-detailt">                  
                </div>
                <div class="news-container">
                 <div class="container">
                     <div  class="row"  style="background:#ffffff;">
                         <div  class="col-md-3"  >
                              <img   style="width:267px; height:auto; align-content:center; margin-top: 5px; margin-bottom: 5px;" src='<%#Eval("confile")%>'   alt='<%#Eval("conCompany")%>' />
                         </div>
                         <div  class="col-md-7" style="width:550px;text-align:justify;font-size: 14px; margin-top: 5px; margin-bottom: 5px;"  >
                              <%#Eval("conDetail")%>
                         </div>
                     </div>
                 </div>                                  
                </div>
                <div class="news-detailt">                  
                </div>

            </ItemTemplate>
        </asp:Repeater>
    </div>
 
    <div class="div-addthis-news">
        <div class="addthis_native_toolbox"></div>
    </div>
    <div class="box-tags-news">
        <asp:Literal ID="ltrTag" runat="server"></asp:Literal>
    </div>
    

    <%if (GlobalClass.commentFNews.Contains("1"))
      {%><div class="shares">

          <div class="fb-comments" data-href="<%=strUrlFace %>" data-colorscheme="<%=cf %>" data-width="<%=wf %>" data-numposts="<%=nf %>"></div>
      
      </div>
    <%} %>      

    <asp:Panel ID="pnNewsOrther" runat="server" Visible="false">
        <div class="clearfix">
            <div class="cate-header sub-top">
                <div class="txt-name-sub">
                    <%= MyWeb.Global.GetLangKey("news_other")%>
                </div>
            </div>
            <asp:Literal runat="server" ID="lrtTinLienQuanKhac"></asp:Literal>
            <ul class="ul-orther-news">
                <asp:Repeater ID="rptNewsOrther" runat="server">
                    <ItemTemplate>                   
                        <li><a href='<%#strLink + (MyWeb.Common.StringClass.NameToTag(Eval("conwebsite").ToString()))+".html" %>'><%#DataBinder.Eval(Container.DataItem, "conName")%> (<%#String.Format("{0:dd/MM/yyyy HH:mm:ss}", Eval("conDate"))%>) </a></li>
                   
                         </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </asp:Panel>
</div>