﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="search_results.aspx.cs" Inherits="MyWeb.search_results" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="loading-body">
            <div class="cate-header sub-top">
                <div class="txt-name-sub">
                    <%--<%=MyWeb.Global.GetLangKey("search_result") %>--%>
                    <asp:Literal ID="ltrHead" runat="server"></asp:Literal>
                </div>
            </div>
            <asp:Literal ID="ltrProList" runat="server"></asp:Literal>
            <div class="clearfix">
                <asp:Literal ID="ltrPaging" runat="server"></asp:Literal>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function () {
                var listChk = "<%=att_level_result %>";
                var key = "<%=key_result %>";
                var viewby = "<%=viewBy_result %>";
                var page = "<%=currentPage %>";
                var opt = "<%=sortBy_result %>";

                $(".next_page").click(function () {
                    page = $(this).attr('rel');
                    $("#loading-body").load("/search_results.aspx?attid=" + listChk + "&key=" + key + "&sort=" + opt + "&view=" + viewby + "&page=" + page);
                });

                $(".first_result").click(function () {
                    page = "1";
                    $("#loading-body").load("/search_results.aspx?attid=" + listChk + "&key=" + key + "&sort=" + opt + "&view=" + viewby + "&page=" + page);
                });

                $(".last_result").click(function () {
                    page = "<%=intPageNumber %>";
                    $("#loading-body").load("/search_results.aspx?attid=" + listChk + "&key=" + key + "&sort=" + opt + "&view=" + viewby + "&page=" + page);
                });

                $(".prev_result").click(function () {
                    page = parseInt(page) - 1;
                    $("#loading-body").load("/search_results.aspx?attid=" + listChk + "&key=" + key + "&sort=" + opt + "&view=" + viewby + "&page=" + page);
                });

                $(".next_result").click(function () {
                    page = parseInt(page) + 1;
                    $("#loading-body").load("/search_results.aspx?attid=" + listChk + "&key=" + key + "&sort=" + opt + "&view=" + viewby + "&page=" + page);
                });
            });

        </script>
    </form>
</body>
</html>
