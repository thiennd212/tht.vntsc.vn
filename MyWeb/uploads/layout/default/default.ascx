﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Register Src="~/views/control/ucLoadControl.ascx" TagPrefix="uc1" TagName="ucLoadControl" %>
<%@ Register Src="~/views/control/ucBanner.ascx" TagPrefix="uc2" TagName="ucBanner" %>
<%@ Register Src="~/views/control/ucSearchBox.ascx" TagPrefix="uc3" TagName="ucSearchBox" %>
<%@ Register Src="~/views/control/ucMenuTop.ascx" TagPrefix="uc4" TagName="ucMenuTop" %>
<%@ Register Src="~/views/control/ucLanguage.ascx" TagPrefix="uc5" TagName="ucLanguage" %>
<%@ Register Src="~/views/control/ucLinkAcount.ascx" TagPrefix="uc6" TagName="ucLinkAcount" %>
<%@ Register Src="~/views/control/ucShopCart.ascx" TagPrefix="uc7" TagName="ucShopCart" %>
<%@ Register Src="~/views/control/ucMenuMain.ascx" TagPrefix="uc8" TagName="ucMenuMain" %>
<%@ Register Src="~/views/control/ucSlides.ascx" TagPrefix="uc9" TagName="ucSlides" %>
<%@ Register Src="~/views/control/ucLogin.ascx" TagPrefix="uc10" TagName="ucLogin" %>
<%@ Register Src="~/views/control/ucOnline.ascx" TagPrefix="uc12" TagName="ucOnline" %>
<%@ Register Src="~/views/control/ucBoxface.ascx" TagPrefix="uc16" TagName="ucBoxface" %>
<%@ Register Src="~/views/control/ucAdvright.ascx" TagPrefix="uc18" TagName="ucAdvright" %>
<%@ Register Src="~/views/news/ucAboutUs.ascx" TagPrefix="uc20" TagName="ucAboutUs" %>
<%@ Register Src="~/views/products/ucProHome.ascx" TagPrefix="uc21" TagName="ucProHome" %>
<%@ Register Src="~/views/news/ucNewsHome.ascx" TagPrefix="uc24" TagName="ucNewsHome" %>

<%@ Register Src="~/views/control/ucMenuFooter.ascx" TagPrefix="uc25" TagName="ucMenuFooter" %>
<%@ Register Src="~/views/control/ucSocialNetwork.ascx" TagPrefix="uc26" TagName="ucSocialNetwork" %>
<%@ Register Src="~/views/control/ucFooter.ascx" TagPrefix="uc27" TagName="ucFooter" %>
<%@ Register Src="~/views/control/ucMenuLeft.ascx" TagPrefix="uc28" TagName="ucMenuLeft" %>
<%@ Register Src="~/views/control/ucMenuLeft.ascx" TagPrefix="uc55" TagName="ucMenuLeft2" %>

<%@ Register Src="~/views/products/ucFilterAttributes.ascx" TagPrefix="uc29" TagName="ucFilterAttributes" %>
<%@ Register Src="~/views/control/ucAdvCenter.ascx" TagPrefix="uc30" TagName="ucAdvCenter" %>
<%@ Register Src="~/views/products/ucProHot.ascx" TagPrefix="uc32" TagName="ucProHot" %>
<%@ Register Src="~/views/control/ucBreadcrumb.ascx" TagPrefix="uc34" TagName="ucBreadcrumb" %>
<%@ Register Src="~/views/control/ucNewsLetter.ascx" TagPrefix="uc36" TagName="ucNewsLetter" %>
<%@ Register Src="~/views/news/ucPriority.ascx" TagPrefix="uc38" TagName="ucPriority" %>
<%@ Register Src="~/views/pages/ucBookNow.ascx" TagPrefix="uc100" TagName="ucBookNow" %>
<%@ Register Src="~/views/products/ucProHomeV2.ascx" TagPrefix="uc1009" TagName="ucProHomeV2" %>
<%@ Register Src="~/views/control/ucSlidesFooter.ascx" TagPrefix="uc1010" TagName="ucSlidesFooter" %>
<%@ Register Src="~/views/products/ucFilterManufac.ascx" TagPrefix="uc1011" TagName="ucFilterManufac" %>
<%@ Register Src="~/views/control/ucSearchLinkOrder.ascx" TagPrefix="uc1012" TagName="ucSearchLinkOrder" %>
<%@ Register Src="~/views/project/ucProjectHot.ascx" TagPrefix="uc1005" TagName="ucProjectHot" %>
<%@ Register Src="~/views/project/ucProjectSale.ascx" TagPrefix="uc1006" TagName="ucProjectSale" %>
<%@ Register Src="~/views/project/ucProjectRegister.ascx" TagPrefix="uc1007" TagName="ucProjectRegister" %>




<div class="menu_close"></div>
<div class="container-fluid top">
    <div class="container">
        <div class="row">
            <div class="col-md-4 phone_top">
                <p><img src="/uploads/layout/default/css/images/phone_top.png" alt=""/> HOTLINE TƯ VẤN TRỰC TUYẾN: <strong>0972 888 666</strong></p>
            </div>
            <div class="col-md-8 internet_all">
                <uc5:ucLanguage runat="server" ID="ucLanguage" />
                <uc26:ucSocialNetwork runat="server" ID="ucSocialNetwork" />
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<header class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-2 banner_all">
                <uc2:ucBanner runat="server" ID="ucBanner" />
            </div>
            <div class="col-md-10 menu_all">
                <div class="menu_pc"><uc8:ucMenuMain runat="server" ID="ucMenuMain" /></div>
                <div class="menu_mobile"><i class="fa fa-bars" aria-hidden="true"> Menu</i></div>
            </div>
        </div>
    </div>
</header>
<div class="slider">
    <uc9:ucSlides runat="server" ID="ucSlides" />
</div>
<%if (Session["home_page"] != null)
{%>
<div class="clearfix"></div>
<div class="container-fluid about_and_home">
    <div class="container">
        <div class="row">
            <div class="col-md-6 about_us"><uc20:ucAboutUs runat="server" ID="ucAboutUs" /></div>
            <div class="col-md-6 news_home"><uc24:ucNewsHome runat="server" ID="ucNewsHome" /></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="container-fluid why_choose_tht lang_vi">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="top_why">
                    <div class="title">
                        Tại sao lại chọn THT Land
                    </div>
                    <div class="strike"></div>
                    <p>Hàng ngày, hàng giờ, THT luôn nỗ lực không ngừng để xây dựng giá trị đích thực của mình trong việc cung cấp những sản phẩm phù hợp nhất </p>
                    <p>cho khách hàng. THT luôn phấn đấu trở thành thương hiệu mạnh trong ngành, đóng góp một phần nhỏ </p>
                    <p>vào công cuộc phát triển đất nước.</p>
                </div>
            </div>
            <div class="col-md-6 list_why">
                <div class="img_why"><img src="/uploads/layout/default/css/images/why_1.png" alt=""/></div>
                <div class="content_why">
                    <div class="title_why">Lịch sử hình thành và phát triển</div>
                    <div class="word_why">Ngày 09 tháng 10 năm 2009, Công ty Cổ phần Đầu Tư Kinh Doanh Bất Động Sản THT – THTLand đã chính thức ra mắt với tên giao dịch nước ngoài là INVESTMENT COMPANY REAL ESTATE BUSINESS THT.</div>
                </div>
            </div>
            <div class="col-md-6 list_why">
                <div class="img_why"><img src="/uploads/layout/default/css/images/why_2.png" alt=""/></div>
                <div class="content_why">
                    <div class="title_why">Triết lí kinh doanh</div>
                    <div class="word_why">Sàn bất động sản THT đã trở thành một nơi tin cậy trong việc cung cấp các dịch vụ tư vấn về mua bán, chuyển nhượng, cho thuê, định giá, môi giới các sản phẩm bất động sản ở các phân khúc khác nhau</div>
                </div>
            </div>
            <div class="col-md-6 list_why">
                <div class="img_why"><img src="/uploads/layout/default/css/images/why_3.png" alt=""/></div>
                <div class="content_why">
                    <div class="title_why">Giá trị THT Land</div>
                    <div class="word_why">Tại THTLand, chúng tôi hiểu rằng để ổn định và phát triển thì mọi thành viên đều phải thấu hiểu những mục tiêu mà mình đang theo đuổi. Cùng cam kết gắn bó và cống hiến cho sự phát triển chung</div>
                </div>
            </div>
            <div class="col-md-6 list_why">
                <div class="img_why"><img src="/uploads/layout/default/css/images/why_4.png" alt=""/></div>
                <div class="content_why">
                    <div class="title_why">Tầm nhìn - Sứ mệnh</div>
                    <div class="word_why">Sứ mệnh của chúng tôi là không ngừng nâng cao mọi chất lượng dịch vụ và sản phẩm để phục vụ khách hàng, đối tác.</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid why_choose_tht lang_en">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="top_why">
                    <div class="title">
                        Why choose THT Land
                    </div>
                    <div class="strike"></div>
                    <p>Every day, every hour, THT always strives to build up its true value in providing the most suitable products.</p>
                    <p>For customers, THT always strives to become a strong brand in the industry, contributing a small part</p>
                    <p>to the development of the country.</p>
                </div>
            </div>
            <div class="col-md-6 list_why">
                <div class="img_why"><img src="/uploads/layout/default/css/images/why_1.png" alt=""/></div>
                <div class="content_why">
                    <div class="title_why">History of formation and development</div>
                    <div class="word_why">On October 09, 2009, THTLand Real Estate Investment Joint Stock Company was officially launched under the name INVESTMENT COMPANY REAL ESTATE BUSINESS THT.</div>
                </div>
            </div>
            <div class="col-md-6 list_why">
                <div class="img_why"><img src="/uploads/layout/default/css/images/why_2.png" alt=""/></div>
                <div class="content_why">
                    <div class="title_why">Business philosophy</div>
                    <div class="word_why">THT real estate has become a trusted place to provide consultancy services on the buying, selling, transferring, leasing, valuation and brokerage of real estate products in different segments.</div>
                </div>
            </div>
            <div class="col-md-6 list_why">
                <div class="img_why"><img src="/uploads/layout/default/css/images/why_3.png" alt=""/></div>
                <div class="content_why">
                    <div class="title_why">Worth THT Land</div>
                    <div class="word_why">At THTLand, we understand that for stability and development, every member must understand the goals that he is pursuing. Commit to stick to and contribute to the overall development</div>
                </div>
            </div>
            <div class="col-md-6 list_why">
                <div class="img_why"><img src="/uploads/layout/default/css/images/why_4.png" alt=""/></div>
                <div class="content_why">
                    <div class="title_why">Vision - Mission</div>
                    <div class="word_why">Our mission is to constantly improve every service and product quality to serve customers, partners.</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="container-fluid project_all">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <uc1005:ucProjectHot runat="server" ID="ucProjectHot" />
            </div>
        </div>
    </div>
</div>
<%} %>
<%if (Session["home_page"] == null)
{%>
    <div class="crum_all">
        <div class="container"><uc34:ucBreadcrumb runat="server" ID="ucBreadcrumb" /></div>
    </div>
    <div class="clearfix"></div>
    <div class="container-fluid main_else">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <uc1:ucLoadControl runat="server" ID="ucLoadControl" />
                    <div class="regis_project"><uc1007:ucProjectRegister runat="server" ID="ucProjectRegister" /></div>
                    <div class="new_prio"><uc38:ucPriority runat="server" ID="ucPriority" /></div>
                </div>
            </div>
        </div>
    </div>
<%} %>
<div class="clearfix"></div>
<div class="container-fluid slider_footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <uc1010:ucSlidesFooter runat="server" ID="ucSlidesFooter" />
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="container-fluid send_email">
    <div class="container">
        <div class="row">
            <div class="col-md-6 left">
                <div class="title"><img src="/uploads/layout/default/css/images/download.png" alt=""/>dowload profile THT land</div>
            </div>
            <div class="col-md-6 right"><uc36:ucNewsLetter runat="server" ID="ucNewsLetter" /></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="maps_footer"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5581.808389344601!2d105.75615871617053!3d20.976187969237188!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313453220f190f4f%3A0x2930709e5e48888e!2zVOG6rXAgxJBvw6BuIFRIVA!5e0!3m2!1svi!2s!4v1513330345786" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe></div>

<footer class='container-fluid'>
    <div class="container">
        <div class="row">
            <div class="col-md-6 footer_left"><uc27:ucFooter runat="server" ID="ucFooter" /></div>
            <div class="col-md-6 footer_right"><uc25:ucMenuFooter runat="server" ID="ucMenuFooter" /></div>
            <div class="col-md-12 title_footer">
                <div class="child">
                    <div class="child_1">
                        Copyright © 2017 CÔNG TY CP ĐẦU TƯ KINH DOANH BẤT ĐỘNG SẢN THT
                    </div>
                    <div class="child_2">
                        Designed by <a href="" target='_blank'>vntsc.vn</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
                   
<div class="to_top"><i class="fa fa-angle-up" aria-hidden="true"></i></div>



<script type="text/javascript">
    $(document).ready(function () {
        $("#content div").hide(); // Initially hide all content
        $("#tabs li:first").attr("id", "current"); // Activate first tab
        $("#content div:first").fadeIn(); // Show first tab content

        $('#tabs a').click(function (e) {
            e.preventDefault();
            if ($(this).closest("li").attr("id") == "current") { //detection for current tab
                return
            }
            else {
                $("#content div").hide(); //Hide all content
                $("#tabs li").attr("id", ""); //Reset id's
                $(this).parent().attr("id", "current"); // Activate this
                $('#' + $(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });
    });

    var owl_category_1 = $(".box-product-hot .body-pro");
    owl_category_1.owlCarousel({
        items: 5,
        loop: true,
        autoPlay: false,
        transitionStyle: "fade",
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [769, 2],
        itemsTablet: [641, 2],
        itemsTablet: [640, 2],
        itemsMobile: [320, 1],
        navigation: true,
        navigationText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
        rewindNav: true,
        scrollPerPage: true,
        slideSpeed: 1500,
        pagination: true,
        paginationNumbers: false,
    });

     var owl_category_2 = $(".detail_box_2 .product-list");
    owl_category_2.owlCarousel({
        items: 5,
        loop: true,
        autoPlay: true,
        transitionStyle: "fade",
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [991, 4],
        itemsTablet: [769, 3],
        itemsTablet: [641, 2],
        itemsTablet: [640, 2],
        itemsMobile: [320, 1],
        navigation: true,
        navigationText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
        rewindNav: true,
        scrollPerPage: true,
        slideSpeed: 1500,
        pagination: true,
        paginationNumbers: false,
    });


    var owl_category3 = $(".listProject_all .listProject_list");
    owl_category3.owlCarousel({
        items: 3,
        autoPlay: true,
        transitionStyle: "fade",
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 2],
        itemsTablet: [640, 1],
        itemsMobile: [320, 1],
        navigation: true,
        navigationText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
        rewindNav: true,
        scrollPerPage: true,
        slideSpeed: 1500,
        pagination: false,
        paginationNumbers: false,
    });

    $(document).ready(function () {
        $(".slides .owl-prev").html('<i class="fa fa-angle-left" aria-hidden="true"></i>');
        $(".slides .owl-next").html('<i class="fa fa-angle-right" aria-hidden="true"></i>');
    });
</script>
